<?php
// HTTP
define('HTTP_SERVER', 'http://supplyshop-latest.n3.iworklab.com/admin/');
define('HTTP_CATALOG', 'http://supplyshop-latest.n3.iworklab.com/');

// HTTPS
define('HTTPS_SERVER', 'http://supplyshop-latest.n3.iworklab.com/admin/');
define('HTTPS_CATALOG', 'http://supplyshop-latest.n3.iworklab.com/');

// DIR
define('DIR_APPLICATION', '/home/bitu/web/supplyshop-latest.n3.iworklab.com/public_html/admin/');
define('DIR_SYSTEM', '/home/bitu/web/supplyshop-latest.n3.iworklab.com/public_html/system/');
define('DIR_IMAGE', '/home/bitu/web/supplyshop-latest.n3.iworklab.com/public_html/image/');
define('DIR_STORAGE', DIR_SYSTEM . 'storage/');
define('DIR_CATALOG', '/home/bitu/web/supplyshop-latest.n3.iworklab.com/public_html/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'bitu_supplyshop');
define('DB_PASSWORD', 'supplyshop@123');
define('DB_DATABASE', 'bitu_supplyshop-latest');
define('DB_PORT', '3306');
define('DB_PREFIX', 'sp_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
