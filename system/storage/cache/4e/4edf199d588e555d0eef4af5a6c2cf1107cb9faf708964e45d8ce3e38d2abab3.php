<?php

/* journal2/template/journal2/module/blog_side_posts.twig */
class __TwigTemplate_7ffaadff6c16ec0fec4d819822f26880a801418d3a93fc3db9970bbf98ed58b1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"journal-blog-side-posts-";
        echo (isset($context["module"]) ? $context["module"] : null);
        echo "\" class=\"journal-blog-side-posts-";
        echo (isset($context["module_id"]) ? $context["module_id"] : null);
        echo " box side-blog side-posts\">
  <div class=\"box-heading\">";
        // line 2
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</div>
  <div class=\"box-post\">
    ";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["posts"]) ? $context["posts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 5
            echo "      <div class=\"side-post\">
        ";
            // line 6
            if ($this->getAttribute($context["post"], "image", array())) {
                // line 7
                echo "          <a class=\"side-post-image\" href=\"";
                echo $this->getAttribute($context["post"], "href", array());
                echo "\"><img src=\"";
                echo $this->getAttribute($context["post"], "image", array());
                echo "\" alt=\"";
                echo $this->getAttribute($context["post"], "name", array());
                echo "\"/></a>
        ";
            }
            // line 9
            echo "        <div class=\"side-post-details\">
          <a class=\"side-post-title\" href=\"";
            // line 10
            echo $this->getAttribute($context["post"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["post"], "name", array());
            echo "</a>
          <div class=\"comment-date\">
            <span class=\"p-date\">";
            // line 12
            echo $this->getAttribute($context["post"], "date", array());
            echo "</span>
            <span class=\"p-comment\">";
            // line 13
            echo $this->getAttribute($context["post"], "comments", array());
            echo "</span>
          </div>
        </div>
      </div>
      <hr/>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "  </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "journal2/template/journal2/module/blog_side_posts.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 19,  64 => 13,  60 => 12,  53 => 10,  50 => 9,  40 => 7,  38 => 6,  35 => 5,  31 => 4,  26 => 2,  19 => 1,);
    }
}
/* <div id="journal-blog-side-posts-{{ module }}" class="journal-blog-side-posts-{{ module_id }} box side-blog side-posts">*/
/*   <div class="box-heading">{{ heading_title }}</div>*/
/*   <div class="box-post">*/
/*     {% for post in posts %}*/
/*       <div class="side-post">*/
/*         {% if post.image %}*/
/*           <a class="side-post-image" href="{{ post.href }}"><img src="{{ post.image }}" alt="{{ post.name }}"/></a>*/
/*         {% endif %}*/
/*         <div class="side-post-details">*/
/*           <a class="side-post-title" href="{{ post.href }}">{{ post.name }}</a>*/
/*           <div class="comment-date">*/
/*             <span class="p-date">{{ post.date }}</span>*/
/*             <span class="p-comment">{{ post.comments }}</span>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*       <hr/>*/
/*     {% endfor %}*/
/*   </div>*/
/* </div>*/
/* */
