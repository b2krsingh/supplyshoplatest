<?php

/* extension/payment/eway.twig */
class __TwigTemplate_15063b4d19fdebb64c1c8f9255e7d7ae1f64c97b7ef2d2d54a3c75b71d0db8f8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"submit\" form=\"form-payment\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo (isset($context["button_save"]) ? $context["button_save"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 7
        echo (isset($context["cancel"]) ? $context["cancel"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_cancel"]) ? $context["button_cancel"] : null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a>
      </div>
      <h1>";
        // line 9
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
\t  <ul class=\"breadcrumb\">
        ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 12
            echo "          <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 14
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
\t";
        // line 18
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 19
            echo "\t  <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
\t    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
\t  </div>
\t";
        }
        // line 23
        echo "    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 25
        echo (isset($context["text_edit"]) ? $context["text_edit"] : null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <form action=\"";
        // line 28
        echo (isset($context["action"]) ? $context["action"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-payment\" class=\"form-horizontal\">
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-paymode\">";
        // line 30
        echo (isset($context["entry_paymode"]) ? $context["entry_paymode"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"payment_eway_paymode\" id=\"input-test\" class=\"form-control\">
\t\t\t\t";
        // line 33
        if (((isset($context["payment_eway_paymode"]) ? $context["payment_eway_paymode"] : null) == "iframe")) {
            // line 34
            echo "\t\t\t\t  <option value=\"iframe\" selected=\"selected\">";
            echo (isset($context["text_iframe"]) ? $context["text_iframe"] : null);
            echo "</option>
\t\t\t\t  <option value=\"transparent\">";
            // line 35
            echo (isset($context["text_transparent"]) ? $context["text_transparent"] : null);
            echo "</option>
\t\t\t\t";
        } else {
            // line 37
            echo "\t\t\t\t  <option value=\"iframe\">";
            echo (isset($context["text_iframe"]) ? $context["text_iframe"] : null);
            echo "</option>
\t\t\t\t  <option value=\"transparent\" selected=\"selected\">";
            // line 38
            echo (isset($context["text_transparent"]) ? $context["text_transparent"] : null);
            echo "</option>
\t\t\t\t";
        }
        // line 40
        echo "              </select>
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-test\"><span data-toggle=\"tooltip\" title=\"";
        // line 44
        echo (isset($context["help_testmode"]) ? $context["help_testmode"] : null);
        echo "\">";
        echo (isset($context["entry_test"]) ? $context["entry_test"] : null);
        echo "</span></label>
            <div class=\"col-sm-10\">
              <select name=\"payment_eway_test\" id=\"input-test\" class=\"form-control\">
\t\t\t\t";
        // line 47
        if ((isset($context["payment_eway_test"]) ? $context["payment_eway_test"] : null)) {
            // line 48
            echo "\t\t\t\t  <option value=\"1\" selected=\"selected\">";
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "</option>
\t\t\t\t  <option value=\"0\">";
            // line 49
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "</option>
\t\t\t\t";
        } else {
            // line 51
            echo "\t\t\t\t  <option value=\"1\">";
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "</option>
\t\t\t\t  <option value=\"0\" selected=\"selected\">";
            // line 52
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "</option>
\t\t\t\t";
        }
        // line 54
        echo "              </select>
            </div>
          </div>
          <div class=\"form-group required\">
            <label class=\"col-sm-2 control-label\" for=\"input-username\"><span data-toggle=\"tooltip\" title=\"";
        // line 58
        echo (isset($context["help_username"]) ? $context["help_username"] : null);
        echo "\">";
        echo (isset($context["entry_username"]) ? $context["entry_username"] : null);
        echo "</span></label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"payment_eway_username\" value=\"";
        // line 60
        echo (isset($context["payment_eway_username"]) ? $context["payment_eway_username"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_username"]) ? $context["entry_username"] : null);
        echo "\" id=\"input-username\" class=\"form-control\"/>
\t\t\t  ";
        // line 61
        if ((isset($context["error_username"]) ? $context["error_username"] : null)) {
            // line 62
            echo "\t\t\t    <div class=\"text-danger\">";
            echo (isset($context["error_username"]) ? $context["error_username"] : null);
            echo "</div>
\t\t\t  ";
        }
        // line 64
        echo "            </div>
          </div>
          <div class=\"form-group required\">
            <label class=\"col-sm-2 control-label\" for=\"input-password\"><span data-toggle=\"tooltip\" title=\"";
        // line 67
        echo (isset($context["help_password"]) ? $context["help_password"] : null);
        echo "\">";
        echo (isset($context["entry_password"]) ? $context["entry_password"] : null);
        echo "</span></label>
            <div class=\"col-sm-10\">
              <input type=\"password\" name=\"payment_eway_password\" value=\"";
        // line 69
        echo (isset($context["payment_eway_password"]) ? $context["payment_eway_password"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_password"]) ? $context["entry_password"] : null);
        echo "\" id=\"input-password\" class=\"form-control\"/>
\t\t\t  ";
        // line 70
        if ((isset($context["error_password"]) ? $context["error_password"] : null)) {
            // line 71
            echo "\t\t\t    <div class=\"text-danger\">";
            echo (isset($context["error_password"]) ? $context["error_password"] : null);
            echo "</div>
\t\t\t  ";
        }
        // line 73
        echo "            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 76
        echo (isset($context["entry_status"]) ? $context["entry_status"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"payment_eway_status\" id=\"input-status\" class=\"form-control\">
\t\t\t\t";
        // line 79
        if ((isset($context["payment_eway_status"]) ? $context["payment_eway_status"] : null)) {
            // line 80
            echo "\t\t\t\t  <option value=\"1\" selected=\"selected\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
\t\t\t\t  <option value=\"0\">";
            // line 81
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
\t\t\t\t";
        } else {
            // line 83
            echo "\t\t\t\t  <option value=\"1\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
\t\t\t\t  <option value=\"0\" selected=\"selected\">";
            // line 84
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
\t\t\t\t";
        }
        // line 86
        echo "              </select>
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-transaction-method\"><span data-toggle=\"tooltip\" title=\"";
        // line 90
        echo (isset($context["help_transaction_method"]) ? $context["help_transaction_method"] : null);
        echo "\">";
        echo (isset($context["entry_transaction_method"]) ? $context["entry_transaction_method"] : null);
        echo "</span></label>
            <div class=\"col-sm-10\">
              <select name=\"payment_eway_transaction_method\" id=\"input-transaction-method\" class=\"form-control\">
\t\t\t\t";
        // line 93
        if (((isset($context["payment_eway_transaction_method"]) ? $context["payment_eway_transaction_method"] : null) == "auth")) {
            // line 94
            echo "\t\t\t\t  <option value=\"payment\">";
            echo (isset($context["text_sale"]) ? $context["text_sale"] : null);
            echo "</option>
\t\t\t\t  <option value=\"auth\" selected=\"selected\">";
            // line 95
            echo (isset($context["text_authorisation"]) ? $context["text_authorisation"] : null);
            echo "</option>
\t\t\t\t";
        } else {
            // line 97
            echo "\t\t\t\t  <option value=\"payment\" selected=\"selected\">";
            echo (isset($context["text_sale"]) ? $context["text_sale"] : null);
            echo "</option>
\t\t\t\t  <option value=\"auth\">";
            // line 98
            echo (isset($context["text_authorisation"]) ? $context["text_authorisation"] : null);
            echo "</option>
\t\t\t\t";
        }
        // line 100
        echo "              </select>
            </div>
          </div>
          <div class=\"form-group required\">
            <label class=\"col-sm-2 control-label\" for=\"eway_payment_type\">";
        // line 104
        echo (isset($context["entry_payment_type"]) ? $context["entry_payment_type"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <p>
                <input type=\"hidden\" name=\"payment_eway_payment_type[visa]\" value=\"0\" />
                <input type='checkbox' name='payment_eway_payment_type[visa]' id=\"eway_payment_type\" value='1' ";
        // line 108
        echo ((($this->getAttribute((isset($context["payment_eway_payment_type"]) ? $context["payment_eway_payment_type"] : null), "visa", array(), "any", true, true) && ($this->getAttribute((isset($context["payment_eway_payment_type"]) ? $context["payment_eway_payment_type"] : null), "visa", array()) == 1))) ? ("checked=\"checked\"") : (""));
        echo " /> CC - Visa
              </p>
              <p>
                <input type=\"hidden\" name=\"payment_eway_payment_type[mastercard]\" value=\"0\" />
                <input type='checkbox' name='payment_eway_payment_type[mastercard]' value='1' ";
        // line 112
        echo ((($this->getAttribute((isset($context["payment_eway_payment_type"]) ? $context["payment_eway_payment_type"] : null), "mastercard", array(), "any", true, true) && ($this->getAttribute((isset($context["payment_eway_payment_type"]) ? $context["payment_eway_payment_type"] : null), "mastercard", array()) == 1))) ? ("checked=\"checked\"") : (""));
        echo " /> CC - MasterCard
              </p>
              <p>
                <input type=\"hidden\" name=\"payment_eway_payment_type[amex]\" value=\"0\" />
                <input type='checkbox' name='payment_eway_payment_type[amex]' value='1' ";
        // line 116
        echo ((($this->getAttribute((isset($context["payment_eway_payment_type"]) ? $context["payment_eway_payment_type"] : null), "amex", array(), "any", true, true) && ($this->getAttribute((isset($context["payment_eway_payment_type"]) ? $context["payment_eway_payment_type"] : null), "amex", array()) == 1))) ? ("checked=\"checked\"") : (""));
        echo " /> CC - Amex
              </p>
              <p>
                <input type=\"hidden\" name=\"payment_eway_payment_type[diners]\" value=\"0\" />
                <input type='checkbox' name='payment_eway_payment_type[diners]' value='1' ";
        // line 120
        echo ((($this->getAttribute((isset($context["payment_eway_payment_type"]) ? $context["payment_eway_payment_type"] : null), "diners", array(), "any", true, true) && ($this->getAttribute((isset($context["payment_eway_payment_type"]) ? $context["payment_eway_payment_type"] : null), "diners", array()) == 1))) ? ("checked=\"checked\"") : (""));
        echo " /> CC - Diners Club
              </p>
              <p>
                <input type=\"hidden\" name=\"payment_eway_payment_type[jcb]\" value=\"0\" />
                <input type='checkbox' name='payment_eway_payment_type[jcb]' value='1' ";
        // line 124
        echo ((($this->getAttribute((isset($context["payment_eway_payment_type"]) ? $context["payment_eway_payment_type"] : null), "jcb", array(), "any", true, true) && ($this->getAttribute((isset($context["payment_eway_payment_type"]) ? $context["payment_eway_payment_type"] : null), "jcb", array()) == 1))) ? ("checked=\"checked\"") : (""));
        echo " /> CC - JCB
              </p>
              <p>
                <input type=\"hidden\" name=\"payment_eway_payment_type[paypal]\" value=\"0\" />
                <input type='checkbox' name='payment_eway_payment_type[paypal]' value='1' ";
        // line 128
        echo ((($this->getAttribute((isset($context["payment_eway_payment_type"]) ? $context["payment_eway_payment_type"] : null), "paypal", array(), "any", true, true) && ($this->getAttribute((isset($context["payment_eway_payment_type"]) ? $context["payment_eway_payment_type"] : null), "paypal", array()) == 1))) ? ("checked=\"checked\"") : (""));
        echo " /> PayPal
              </p>
              <p>
                <input type=\"hidden\" name=\"payment_eway_payment_type[masterpass]\" value=\"0\" />
                <input type='checkbox' name='payment_eway_payment_type[masterpass]' value='1' ";
        // line 132
        echo ((($this->getAttribute((isset($context["payment_eway_payment_type"]) ? $context["payment_eway_payment_type"] : null), "masterpass", array(), "any", true, true) && ($this->getAttribute((isset($context["payment_eway_payment_type"]) ? $context["payment_eway_payment_type"] : null), "masterpass", array()) == 1))) ? ("checked=\"checked\"") : (""));
        echo " /> MasterPass
              </p>
\t\t\t  ";
        // line 134
        if ((isset($context["error_payment_type"]) ? $context["error_payment_type"] : null)) {
            // line 135
            echo "\t\t\t    <div class=\"text-danger\">";
            echo (isset($context["error_payment_type"]) ? $context["error_payment_type"] : null);
            echo "</div>
\t\t\t  ";
        }
        // line 137
        echo "            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-geo-zone\">";
        // line 140
        echo (isset($context["entry_geo_zone"]) ? $context["entry_geo_zone"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"payment_eway_standard_geo_zone_id\" id=\"input-geo-zone\" class=\"form-control\">
                <option value=\"0\">";
        // line 143
        echo (isset($context["text_all_zones"]) ? $context["text_all_zones"] : null);
        echo "</option>
\t\t\t\t";
        // line 144
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["geo_zones"]) ? $context["geo_zones"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["geo_zone"]) {
            // line 145
            echo "\t\t\t\t  ";
            if (($this->getAttribute($context["geo_zone"], "geo_zone_id", array()) == (isset($context["payment_eway_standard_geo_zone_id"]) ? $context["payment_eway_standard_geo_zone_id"] : null))) {
                // line 146
                echo "\t\t\t\t    <option value=\"";
                echo $this->getAttribute($context["geo_zone"], "geo_zone_id", array());
                echo "\" selected=\"selected\">";
                echo $this->getAttribute($context["geo_zone"], "name", array());
                echo "</option>
\t\t\t\t  ";
            } else {
                // line 148
                echo "\t\t\t\t    <option value=\"";
                echo $this->getAttribute($context["geo_zone"], "geo_zone_id", array());
                echo "\">";
                echo $this->getAttribute($context["geo_zone"], "name", array());
                echo "</option>
\t\t\t\t  ";
            }
            // line 150
            echo "\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['geo_zone'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 151
        echo "              </select>
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-order-status\">";
        // line 155
        echo (isset($context["entry_order_status"]) ? $context["entry_order_status"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"payment_eway_order_status_id\" id=\"input-order-status\" class=\"form-control\">
\t\t\t\t";
        // line 158
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["order_statuses"]) ? $context["order_statuses"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["order_status"]) {
            // line 159
            echo "                  ";
            if (($this->getAttribute($context["order_status"], "order_status_id", array()) == (isset($context["payment_eway_order_status_id"]) ? $context["payment_eway_order_status_id"] : null))) {
                // line 160
                echo "\t\t\t\t    <option value=\"";
                echo $this->getAttribute($context["order_status"], "order_status_id", array());
                echo "\" selected=\"selected\">";
                echo $this->getAttribute($context["order_status"], "name", array());
                echo "</option>
\t\t\t\t  ";
            } else {
                // line 162
                echo "\t\t\t\t    <option value=\"";
                echo $this->getAttribute($context["order_status"], "order_status_id", array());
                echo "\">";
                echo $this->getAttribute($context["order_status"], "name", array());
                echo "</option>
\t\t\t\t  ";
            }
            // line 164
            echo "\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 165
        echo "              </select>
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-order-status-refund\">";
        // line 169
        echo (isset($context["entry_order_status_refund"]) ? $context["entry_order_status_refund"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"payment_eway_order_status_refunded_id\" id=\"input-order-status-refund\" class=\"form-control\">
\t\t\t\t";
        // line 172
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["order_statuses"]) ? $context["order_statuses"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["order_status"]) {
            // line 173
            echo "\t\t\t\t  ";
            if (($this->getAttribute($context["order_status"], "order_status_id", array()) == (isset($context["payment_eway_order_status_refunded_id"]) ? $context["payment_eway_order_status_refunded_id"] : null))) {
                // line 174
                echo "\t\t\t\t    <option value=\"";
                echo $this->getAttribute($context["order_status"], "order_status_id", array());
                echo "\" selected=\"selected\">";
                echo $this->getAttribute($context["order_status"], "name", array());
                echo "</option>
\t\t\t\t  ";
            } else {
                // line 176
                echo "\t\t\t\t    <option value=\"";
                echo $this->getAttribute($context["order_status"], "order_status_id", array());
                echo "\">";
                echo $this->getAttribute($context["order_status"], "name", array());
                echo "</option>
\t\t\t\t  ";
            }
            // line 178
            echo "\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 179
        echo "              </select>
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-order-status-auth\">";
        // line 183
        echo (isset($context["entry_order_status_auth"]) ? $context["entry_order_status_auth"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"payment_eway_order_status_auth_id\" id=\"input-order-status-auth\" class=\"form-control\">
\t\t\t\t";
        // line 186
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["order_statuses"]) ? $context["order_statuses"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["order_status"]) {
            // line 187
            echo "\t\t\t\t  ";
            if (($this->getAttribute($context["order_status"], "order_status_id", array()) == (isset($context["payment_eway_order_status_auth_id"]) ? $context["payment_eway_order_status_auth_id"] : null))) {
                // line 188
                echo "\t\t\t\t    <option value=\"";
                echo $this->getAttribute($context["order_status"], "order_status_id", array());
                echo "\" selected=\"selected\">";
                echo $this->getAttribute($context["order_status"], "name", array());
                echo "</option>
\t\t\t\t  ";
            } else {
                // line 190
                echo "\t\t\t\t    <option value=\"";
                echo $this->getAttribute($context["order_status"], "order_status_id", array());
                echo "\">";
                echo $this->getAttribute($context["order_status"], "name", array());
                echo "</option>
\t\t\t\t  ";
            }
            // line 192
            echo "\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 193
        echo "\t\t\t  </select>
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-order-status-fraud\">";
        // line 197
        echo (isset($context["entry_order_status_fraud"]) ? $context["entry_order_status_fraud"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"payment_eway_order_status_fraud_id\" id=\"input-order-status-fraud\" class=\"form-control\">
\t\t\t\t";
        // line 200
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["order_statuses"]) ? $context["order_statuses"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["order_status"]) {
            // line 201
            echo "\t\t\t\t  ";
            if (($this->getAttribute($context["order_status"], "order_status_id", array()) == (isset($context["payment_eway_order_status_fraud_id"]) ? $context["payment_eway_order_status_fraud_id"] : null))) {
                // line 202
                echo "\t\t\t\t    <option value=\"";
                echo $this->getAttribute($context["order_status"], "order_status_id", array());
                echo "\" selected=\"selected\">";
                echo $this->getAttribute($context["order_status"], "name", array());
                echo "</option>
\t\t\t\t  ";
            } else {
                // line 204
                echo "\t\t\t\t    <option value=\"";
                echo $this->getAttribute($context["order_status"], "order_status_id", array());
                echo "\">";
                echo $this->getAttribute($context["order_status"], "name", array());
                echo "</option>
\t\t\t\t  ";
            }
            // line 206
            echo "\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 207
        echo "\t\t\t  </select>
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-sort-order\">";
        // line 211
        echo (isset($context["entry_sort_order"]) ? $context["entry_sort_order"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"payment_eway_sort_order\" value=\"";
        // line 213
        echo (isset($context["payment_eway_sort_order"]) ? $context["payment_eway_sort_order"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_sort_order"]) ? $context["entry_sort_order"] : null);
        echo "\" id=\"input-sort-order\" class=\"form-control\"/>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
";
        // line 221
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "extension/payment/eway.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  576 => 221,  563 => 213,  558 => 211,  552 => 207,  546 => 206,  538 => 204,  530 => 202,  527 => 201,  523 => 200,  517 => 197,  511 => 193,  505 => 192,  497 => 190,  489 => 188,  486 => 187,  482 => 186,  476 => 183,  470 => 179,  464 => 178,  456 => 176,  448 => 174,  445 => 173,  441 => 172,  435 => 169,  429 => 165,  423 => 164,  415 => 162,  407 => 160,  404 => 159,  400 => 158,  394 => 155,  388 => 151,  382 => 150,  374 => 148,  366 => 146,  363 => 145,  359 => 144,  355 => 143,  349 => 140,  344 => 137,  338 => 135,  336 => 134,  331 => 132,  324 => 128,  317 => 124,  310 => 120,  303 => 116,  296 => 112,  289 => 108,  282 => 104,  276 => 100,  271 => 98,  266 => 97,  261 => 95,  256 => 94,  254 => 93,  246 => 90,  240 => 86,  235 => 84,  230 => 83,  225 => 81,  220 => 80,  218 => 79,  212 => 76,  207 => 73,  201 => 71,  199 => 70,  193 => 69,  186 => 67,  181 => 64,  175 => 62,  173 => 61,  167 => 60,  160 => 58,  154 => 54,  149 => 52,  144 => 51,  139 => 49,  134 => 48,  132 => 47,  124 => 44,  118 => 40,  113 => 38,  108 => 37,  103 => 35,  98 => 34,  96 => 33,  90 => 30,  85 => 28,  79 => 25,  75 => 23,  67 => 19,  65 => 18,  59 => 14,  48 => 12,  44 => 11,  39 => 9,  32 => 7,  28 => 6,  19 => 1,);
    }
}
/* {{ header }}{{ column_left }}*/
/* <div id="content">*/
/*   <div class="page-header">*/
/*     <div class="container-fluid">*/
/*       <div class="pull-right">*/
/*         <button type="submit" form="form-payment" data-toggle="tooltip" title="{{ button_save }}" class="btn btn-primary"><i class="fa fa-save"></i></button>*/
/*         <a href="{{ cancel }}" data-toggle="tooltip" title="{{ button_cancel }}" class="btn btn-default"><i class="fa fa-reply"></i></a>*/
/*       </div>*/
/*       <h1>{{ heading_title }}</h1>*/
/* 	  <ul class="breadcrumb">*/
/*         {% for breadcrumb in breadcrumbs %}*/
/*           <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*         {% endfor %}*/
/*       </ul>*/
/*     </div>*/
/*   </div>*/
/*   <div class="container-fluid">*/
/* 	{% if error_warning %}*/
/* 	  <div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/* 	    <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/* 	  </div>*/
/* 	{% endif %}*/
/*     <div class="panel panel-default">*/
/*       <div class="panel-heading">*/
/*         <h3 class="panel-title"><i class="fa fa-pencil"></i> {{ text_edit }}</h3>*/
/*       </div>*/
/*       <div class="panel-body">*/
/*         <form action="{{ action }}" method="post" enctype="multipart/form-data" id="form-payment" class="form-horizontal">*/
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-paymode">{{ entry_paymode }}</label>*/
/*             <div class="col-sm-10">*/
/*               <select name="payment_eway_paymode" id="input-test" class="form-control">*/
/* 				{% if payment_eway_paymode == 'iframe' %}*/
/* 				  <option value="iframe" selected="selected">{{ text_iframe }}</option>*/
/* 				  <option value="transparent">{{ text_transparent }}</option>*/
/* 				{% else %}*/
/* 				  <option value="iframe">{{ text_iframe }}</option>*/
/* 				  <option value="transparent" selected="selected">{{ text_transparent }}</option>*/
/* 				{% endif %}*/
/*               </select>*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-test"><span data-toggle="tooltip" title="{{ help_testmode }}">{{ entry_test }}</span></label>*/
/*             <div class="col-sm-10">*/
/*               <select name="payment_eway_test" id="input-test" class="form-control">*/
/* 				{% if payment_eway_test %}*/
/* 				  <option value="1" selected="selected">{{ text_yes }}</option>*/
/* 				  <option value="0">{{ text_no }}</option>*/
/* 				{% else %}*/
/* 				  <option value="1">{{ text_yes }}</option>*/
/* 				  <option value="0" selected="selected">{{ text_no }}</option>*/
/* 				{% endif %}*/
/*               </select>*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group required">*/
/*             <label class="col-sm-2 control-label" for="input-username"><span data-toggle="tooltip" title="{{ help_username }}">{{ entry_username }}</span></label>*/
/*             <div class="col-sm-10">*/
/*               <input type="text" name="payment_eway_username" value="{{ payment_eway_username }}" placeholder="{{ entry_username }}" id="input-username" class="form-control"/>*/
/* 			  {% if error_username %}*/
/* 			    <div class="text-danger">{{ error_username }}</div>*/
/* 			  {% endif %}*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group required">*/
/*             <label class="col-sm-2 control-label" for="input-password"><span data-toggle="tooltip" title="{{ help_password }}">{{ entry_password }}</span></label>*/
/*             <div class="col-sm-10">*/
/*               <input type="password" name="payment_eway_password" value="{{ payment_eway_password }}" placeholder="{{ entry_password }}" id="input-password" class="form-control"/>*/
/* 			  {% if error_password %}*/
/* 			    <div class="text-danger">{{ error_password }}</div>*/
/* 			  {% endif %}*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-status">{{ entry_status }}</label>*/
/*             <div class="col-sm-10">*/
/*               <select name="payment_eway_status" id="input-status" class="form-control">*/
/* 				{% if payment_eway_status %}*/
/* 				  <option value="1" selected="selected">{{ text_enabled }}</option>*/
/* 				  <option value="0">{{ text_disabled }}</option>*/
/* 				{% else %}*/
/* 				  <option value="1">{{ text_enabled }}</option>*/
/* 				  <option value="0" selected="selected">{{ text_disabled }}</option>*/
/* 				{% endif %}*/
/*               </select>*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-transaction-method"><span data-toggle="tooltip" title="{{ help_transaction_method }}">{{ entry_transaction_method }}</span></label>*/
/*             <div class="col-sm-10">*/
/*               <select name="payment_eway_transaction_method" id="input-transaction-method" class="form-control">*/
/* 				{% if payment_eway_transaction_method == 'auth' %}*/
/* 				  <option value="payment">{{ text_sale }}</option>*/
/* 				  <option value="auth" selected="selected">{{ text_authorisation }}</option>*/
/* 				{% else %}*/
/* 				  <option value="payment" selected="selected">{{ text_sale }}</option>*/
/* 				  <option value="auth">{{ text_authorisation }}</option>*/
/* 				{% endif %}*/
/*               </select>*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group required">*/
/*             <label class="col-sm-2 control-label" for="eway_payment_type">{{ entry_payment_type }}</label>*/
/*             <div class="col-sm-10">*/
/*               <p>*/
/*                 <input type="hidden" name="payment_eway_payment_type[visa]" value="0" />*/
/*                 <input type='checkbox' name='payment_eway_payment_type[visa]' id="eway_payment_type" value='1' {{ payment_eway_payment_type.visa is defined and payment_eway_payment_type.visa == 1 ? 'checked="checked"' : '' }} /> CC - Visa*/
/*               </p>*/
/*               <p>*/
/*                 <input type="hidden" name="payment_eway_payment_type[mastercard]" value="0" />*/
/*                 <input type='checkbox' name='payment_eway_payment_type[mastercard]' value='1' {{ payment_eway_payment_type.mastercard is defined and payment_eway_payment_type.mastercard == 1 ? 'checked="checked"' : '' }} /> CC - MasterCard*/
/*               </p>*/
/*               <p>*/
/*                 <input type="hidden" name="payment_eway_payment_type[amex]" value="0" />*/
/*                 <input type='checkbox' name='payment_eway_payment_type[amex]' value='1' {{ payment_eway_payment_type.amex is defined and payment_eway_payment_type.amex == 1 ? 'checked="checked"' : '' }} /> CC - Amex*/
/*               </p>*/
/*               <p>*/
/*                 <input type="hidden" name="payment_eway_payment_type[diners]" value="0" />*/
/*                 <input type='checkbox' name='payment_eway_payment_type[diners]' value='1' {{ payment_eway_payment_type.diners is defined and payment_eway_payment_type.diners == 1 ? 'checked="checked"' : '' }} /> CC - Diners Club*/
/*               </p>*/
/*               <p>*/
/*                 <input type="hidden" name="payment_eway_payment_type[jcb]" value="0" />*/
/*                 <input type='checkbox' name='payment_eway_payment_type[jcb]' value='1' {{ payment_eway_payment_type.jcb is defined and payment_eway_payment_type.jcb == 1 ? 'checked="checked"' : '' }} /> CC - JCB*/
/*               </p>*/
/*               <p>*/
/*                 <input type="hidden" name="payment_eway_payment_type[paypal]" value="0" />*/
/*                 <input type='checkbox' name='payment_eway_payment_type[paypal]' value='1' {{ payment_eway_payment_type.paypal is defined and payment_eway_payment_type.paypal == 1 ? 'checked="checked"' : '' }} /> PayPal*/
/*               </p>*/
/*               <p>*/
/*                 <input type="hidden" name="payment_eway_payment_type[masterpass]" value="0" />*/
/*                 <input type='checkbox' name='payment_eway_payment_type[masterpass]' value='1' {{ payment_eway_payment_type.masterpass is defined and payment_eway_payment_type.masterpass == 1 ? 'checked="checked"' : '' }} /> MasterPass*/
/*               </p>*/
/* 			  {% if error_payment_type %}*/
/* 			    <div class="text-danger">{{ error_payment_type }}</div>*/
/* 			  {% endif %}*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-geo-zone">{{ entry_geo_zone }}</label>*/
/*             <div class="col-sm-10">*/
/*               <select name="payment_eway_standard_geo_zone_id" id="input-geo-zone" class="form-control">*/
/*                 <option value="0">{{ text_all_zones }}</option>*/
/* 				{% for geo_zone in geo_zones %}*/
/* 				  {% if geo_zone.geo_zone_id == payment_eway_standard_geo_zone_id %}*/
/* 				    <option value="{{ geo_zone.geo_zone_id }}" selected="selected">{{ geo_zone.name }}</option>*/
/* 				  {% else %}*/
/* 				    <option value="{{ geo_zone.geo_zone_id }}">{{ geo_zone.name }}</option>*/
/* 				  {% endif %}*/
/* 				{% endfor %}*/
/*               </select>*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-order-status">{{ entry_order_status }}</label>*/
/*             <div class="col-sm-10">*/
/*               <select name="payment_eway_order_status_id" id="input-order-status" class="form-control">*/
/* 				{% for order_status in order_statuses %}*/
/*                   {% if order_status.order_status_id == payment_eway_order_status_id %}*/
/* 				    <option value="{{ order_status.order_status_id }}" selected="selected">{{ order_status.name }}</option>*/
/* 				  {% else %}*/
/* 				    <option value="{{ order_status.order_status_id }}">{{ order_status.name }}</option>*/
/* 				  {% endif %}*/
/* 				{% endfor %}*/
/*               </select>*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-order-status-refund">{{ entry_order_status_refund }}</label>*/
/*             <div class="col-sm-10">*/
/*               <select name="payment_eway_order_status_refunded_id" id="input-order-status-refund" class="form-control">*/
/* 				{% for order_status in order_statuses %}*/
/* 				  {% if order_status.order_status_id == payment_eway_order_status_refunded_id %}*/
/* 				    <option value="{{ order_status.order_status_id }}" selected="selected">{{ order_status.name }}</option>*/
/* 				  {% else %}*/
/* 				    <option value="{{ order_status.order_status_id }}">{{ order_status.name }}</option>*/
/* 				  {% endif %}*/
/* 				{% endfor %}*/
/*               </select>*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-order-status-auth">{{ entry_order_status_auth }}</label>*/
/*             <div class="col-sm-10">*/
/*               <select name="payment_eway_order_status_auth_id" id="input-order-status-auth" class="form-control">*/
/* 				{% for order_status in order_statuses %}*/
/* 				  {% if order_status.order_status_id == payment_eway_order_status_auth_id %}*/
/* 				    <option value="{{ order_status.order_status_id }}" selected="selected">{{ order_status.name }}</option>*/
/* 				  {% else %}*/
/* 				    <option value="{{ order_status.order_status_id }}">{{ order_status.name }}</option>*/
/* 				  {% endif %}*/
/* 				{% endfor %}*/
/* 			  </select>*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-order-status-fraud">{{ entry_order_status_fraud }}</label>*/
/*             <div class="col-sm-10">*/
/*               <select name="payment_eway_order_status_fraud_id" id="input-order-status-fraud" class="form-control">*/
/* 				{% for order_status in order_statuses %}*/
/* 				  {% if order_status.order_status_id == payment_eway_order_status_fraud_id %}*/
/* 				    <option value="{{ order_status.order_status_id }}" selected="selected">{{ order_status.name }}</option>*/
/* 				  {% else %}*/
/* 				    <option value="{{ order_status.order_status_id }}">{{ order_status.name }}</option>*/
/* 				  {% endif %}*/
/* 				{% endfor %}*/
/* 			  </select>*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-sort-order">{{ entry_sort_order }}</label>*/
/*             <div class="col-sm-10">*/
/*               <input type="text" name="payment_eway_sort_order" value="{{ payment_eway_sort_order }}" placeholder="{{ entry_sort_order }}" id="input-sort-order" class="form-control"/>*/
/*             </div>*/
/*           </div>*/
/*         </form>*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/* </div>*/
/* {{ footer }}*/
