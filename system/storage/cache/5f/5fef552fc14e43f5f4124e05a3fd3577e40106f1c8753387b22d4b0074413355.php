<?php

/* default/template/extension/payment/eway.twig */
class __TwigTemplate_a9c981774d6326a1b0c1f97f90968394f7f0589475541e42877381e20455b441 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["error"]) ? $context["error"] : null)) {
            // line 2
            echo "<div class=\"alert alert-danger alert-dismissible\">Payment Error: ";
            echo (isset($context["error"]) ? $context["error"] : null);
            echo "</div>
";
        } else {
            // line 4
            echo "<form action=\"";
            echo (isset($context["action"]) ? $context["action"] : null);
            echo "\" method=\"POST\" class=\"form-horizontal\" id=\"eway-payment-form\">
  <fieldset id=\"payment\">
    <legend>";
            // line 6
            echo (isset($context["text_credit_card"]) ? $context["text_credit_card"] : null);
            echo "</legend>
    <input type=\"hidden\" name=\"EWAY_ACCESSCODE\" value=\"";
            // line 7
            echo (isset($context["AccessCode"]) ? $context["AccessCode"] : null);
            echo "\" />
    ";
            // line 8
            if ((isset($context["text_testing"]) ? $context["text_testing"] : null)) {
                // line 9
                echo "    <div class=\"alert alert-warning alert-dismissible\">";
                echo (isset($context["text_testing"]) ? $context["text_testing"] : null);
                echo "</div>
    ";
            }
            // line 11
            echo "    <div class=\"form-group\">
      <div class=\"col-sm-12\">
        <ul>
          ";
            // line 14
            if (((((($this->getAttribute((isset($context["payment_type"]) ? $context["payment_type"] : null), "visa", array()) == 1) || ($this->getAttribute((isset($context["payment_type"]) ? $context["payment_type"] : null), "mastercard", array()) == 1)) || ($this->getAttribute((isset($context["payment_type"]) ? $context["payment_type"] : null), "diners", array()) == 1)) || ($this->getAttribute((isset($context["payment_type"]) ? $context["payment_type"] : null), "jcb", array()) == 1)) || ($this->getAttribute((isset($context["payment_type"]) ? $context["payment_type"] : null), "amex", array()) == 1))) {
                // line 15
                echo "          <label>
            <input type=\"radio\" name=\"EWAY_PAYMENTTYPE\" id=\"eway-radio-cc\" value=\"creditcard\" checked=\"checked\" onchange=\"javascript:select_eWAYPaymentOption('creditcard')\" />
            ";
                // line 17
                if (($this->getAttribute((isset($context["payment_type"]) ? $context["payment_type"] : null), "visa", array()) == 1)) {
                    echo " <img src=\"catalog/view/theme/default/image/eway_creditcard_visa.png\" height=\"30\" alt=\"Visa\" /> ";
                }
                // line 18
                echo "            ";
                if (($this->getAttribute((isset($context["payment_type"]) ? $context["payment_type"] : null), "mastercard", array()) == 1)) {
                    echo " <img src=\"catalog/view/theme/default/image/eway_creditcard_master.png\" height=\"30\" alt=\"MasterCard\" /> ";
                }
                // line 19
                echo "            ";
                if (($this->getAttribute((isset($context["payment_type"]) ? $context["payment_type"] : null), "diners", array()) == 1)) {
                    echo " <img src=\"catalog/view/theme/default/image/eway_creditcard_diners.png\" height=\"30\" alt=\"Diners Club\" /> ";
                }
                // line 20
                echo "            ";
                if (($this->getAttribute((isset($context["payment_type"]) ? $context["payment_type"] : null), "jcb", array()) == 1)) {
                    echo " <img src=\"catalog/view/theme/default/image/eway_creditcard_jcb.png\" height=\"30\" alt=\"JCB\" /> ";
                }
                // line 21
                echo "            ";
                if (($this->getAttribute((isset($context["payment_type"]) ? $context["payment_type"] : null), "amex", array()) == 1)) {
                    echo " <img src=\"catalog/view/theme/default/image/eway_creditcard_amex.png\" height=\"30\" alt=\"AMEX\" /> ";
                }
                echo " </label>
          ";
            }
            // line 23
            echo "          ";
            if (($this->getAttribute((isset($context["payment_type"]) ? $context["payment_type"] : null), "paypal", array()) == 1)) {
                // line 24
                echo "          <label>
            <input type=\"radio\" name=\"EWAY_PAYMENTTYPE\" value=\"paypal\" onchange=\"javascript:select_eWAYPaymentOption(paypal)\" />
            <img src=\"catalog/view/theme/default/image/eway_paypal.png\" height=\"30\" alt=\"";
                // line 26
                echo (isset($context["text_card_type_pp"]) ? $context["text_card_type_pp"] : null);
                echo "\" /></label>
          ";
            }
            // line 28
            echo "          ";
            if (($this->getAttribute((isset($context["payment_type"]) ? $context["payment_type"] : null), "masterpass", array()) == 1)) {
                // line 29
                echo "          <label>
            <input type=\"radio\" name=\"EWAY_PAYMENTTYPE\" value=\"masterpass\" onchange=\"javascript:select_eWAYPaymentOption(masterpass)\" />
            <img src=\"catalog/view/theme/default/image/eway_masterpass.png\" height=\"30\" alt=\"";
                // line 31
                echo (isset($context["text_card_type_mp"]) ? $context["text_card_type_mp"] : null);
                echo "\" /></label>
          ";
            }
            // line 33
            echo "        </ul>
      </div>
    </div>
    ";
            // line 36
            if (($this->getAttribute((isset($context["payment_type"]) ? $context["payment_type"] : null), "paypal", array()) == 1)) {
                // line 37
                echo "    <p id=\"tip-paypal\" style=\"display:none;\">";
                echo (isset($context["text_type_help"]) ? $context["text_type_help"] : null);
                echo (isset($context["text_card_type_pp"]) ? $context["text_card_type_pp"] : null);
                echo "</p>
    ";
            }
            // line 39
            echo "    ";
            if (($this->getAttribute((isset($context["payment_type"]) ? $context["payment_type"] : null), "masterpass", array()) == 1)) {
                // line 40
                echo "    <p id=\"tip-masterpass\" style=\"display:none;\">";
                echo (isset($context["text_type_help"]) ? $context["text_type_help"] : null);
                echo (isset($context["text_card_type_mp"]) ? $context["text_card_type_mp"] : null);
                echo "</p>
    ";
            }
            // line 42
            echo "    
        ";
            // line 43
            if (((((($this->getAttribute((isset($context["payment_type"]) ? $context["payment_type"] : null), "visa", array()) == 1) || ($this->getAttribute((isset($context["payment_type"]) ? $context["payment_type"] : null), "mastercard", array()) == 1)) || ($this->getAttribute((isset($context["payment_type"]) ? $context["payment_type"] : null), "diners", array()) == 1)) || ($this->getAttribute((isset($context["payment_type"]) ? $context["payment_type"] : null), "jcb", array()) == 1)) || ($this->getAttribute((isset($context["payment_type"]) ? $context["payment_type"] : null), "amex", array()) == 1))) {
                // line 44
                echo "      
    <div id=\"creditcard-info\">
    <div class=\"form-group required\">
      <label class=\"col-sm-2 control-label\" for=\"eway-cardname\">";
                // line 47
                echo (isset($context["entry_cc_name"]) ? $context["entry_cc_name"] : null);
                echo "</label>
      <div class=\"col-sm-10\">
        <input name=\"EWAY_CARDNAME\" type=\"text\" value=\"\" id=\"eway-cardname\" placeholder=\"";
                // line 49
                echo (isset($context["entry_cc_name"]) ? $context["entry_cc_name"] : null);
                echo "\"  autocomplete=\"off\" class=\"form-control\"/>
        <span id=\"ewaycard-error\" class=\"text-danger\"></span> </div>
    </div>
    <div class=\"form-group required\">
      <label class=\"col-sm-2 control-label\" for=\"eway-cardnumber\">";
                // line 53
                echo (isset($context["entry_cc_number"]) ? $context["entry_cc_number"] : null);
                echo "</label>
      <div class=\"col-sm-10\">
        <input name=\"EWAY_CARDNUMBER\" type=\"text\" maxlength=\"19\" id=\"eway-cardnumber\" value=\"\" placeholder=\"";
                // line 55
                echo (isset($context["entry_cc_number"]) ? $context["entry_cc_number"] : null);
                echo "\"  autocomplete=\"off\" class=\"form-control\" pattern=\"\\d*\" />
        <span id=\"ewaynumber-error\" class=\"text-danger\"></span> </div>
    </div>
    <div class=\"form-group required\">
      <label class=\"col-sm-2 control-label\" for=\"eway-card-expiry-month\">";
                // line 59
                echo (isset($context["entry_cc_expire_date"]) ? $context["entry_cc_expire_date"] : null);
                echo "</label>
      <div class=\"col-sm-2\">
        <select name=\"EWAY_CARDEXPIRYMONTH\" id=\"eway-card-expiry-month\" class=\"form-control\">
          
          
\t\t\t\t\t
        ";
                // line 65
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["months"]) ? $context["months"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["month"]) {
                    // line 66
                    echo "      
\t\t\t\t\t\t
          
          <option value=\"";
                    // line 69
                    echo $this->getAttribute($context["month"], "value", array());
                    echo "\">";
                    echo $this->getAttribute($context["month"], "text", array());
                    echo "</option>
          
          
\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['month'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 73
                echo "\t\t\t\t  
        
        </select>
      </div>
      <div class=\"col-sm-2\">
        <select name=\"EWAY_CARDEXPIRYYEAR\" id=\"eway-card-expiry-year\" class=\"form-control\">
          
          
\t\t\t\t\t
        ";
                // line 82
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["year_expire"]) ? $context["year_expire"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["year"]) {
                    // line 83
                    echo "      
\t\t\t\t\t\t
          
          <option value=\"";
                    // line 86
                    echo $this->getAttribute($context["year"], "value", array());
                    echo "\">";
                    echo $this->getAttribute($context["year"], "text", array());
                    echo "</option>
          
          
\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['year'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 90
                echo "\t\t\t\t  
        
        </select>
        <div id=\"expiry-error\" class=\"text-danger\"></div>
      </div>
    </div>
    <div class=\"form-group required\">
      <label class=\"col-sm-2 control-label\" for=\"eway-cardcvn\">";
                // line 97
                echo (isset($context["entry_cc_cvv2"]) ? $context["entry_cc_cvv2"] : null);
                echo "</label>
      <div class=\"col-sm-10\">
        <input name=\"EWAY_CARDCVN\" type=\"text\" maxlength=\"4\" value=\"\" placeholder=\"";
                // line 99
                echo (isset($context["entry_cc_cvv2"]) ? $context["entry_cc_cvv2"] : null);
                echo "\" id=\"eway-cardcvn\" autocomplete=\"off\" class=\"form-control\" pattern=\"\\d*\" />
        <span id=\"cvn-details\" class=\"help\"> ";
                // line 100
                echo (isset($context["help_cvv"]) ? $context["help_cvv"] : null);
                echo "
        ";
                // line 101
                if (twig_in_filter("amex", (isset($context["payment_type"]) ? $context["payment_type"] : null))) {
                    echo "<br />
        ";
                    // line 102
                    echo (isset($context["help_cvv_amex"]) ? $context["help_cvv_amex"] : null);
                    echo "
        ";
                }
                // line 103
                echo " </span><br />
        <span id=\"ewaycvn-error\" class=\"text-danger\"></span> </div>
    </div>
    ";
            }
            // line 107
            echo "  </fieldset>
</form>
<div class=\"buttons\">
  <div class=\"pull-right\">
    <input type=\"button\" value=\"";
            // line 111
            echo (isset($context["button_confirm"]) ? $context["button_confirm"] : null);
            echo "\" id=\"button-confirm\" data-loading-text=\"";
            echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
            echo "\" class=\"btn btn-primary\" />
  </div>
</div>
<script language=\"JavaScript\" type=\"text/javascript\" >//<!--
\t    function select_eWAYPaymentOption(v) {
\t      if (\$(\"#creditcard-info\").length) {
\t        \$(\"#creditcard-info\").hide();
\t      }
\t      if (\$(\"#tip-paypal\").length) {
\t        \$(\"#tip-paypal\").hide();
\t      }
\t      if (\$(\"#tip-masterpass\").length) {
\t        \$(\"#tip-masterpass\").hide();
\t      }
\t      if (\$(\"#tip-vme\").length) {
\t        \$(\"#tip-vme\").hide();
\t      }
\t      if (v == 'creditcard') {
\t        \$(\"#creditcard-info\").show();
\t      } else {
\t        \$(\"#tip-\" + v).show();
\t      }
\t    }
\t//--></script> 
<script type=\"text/javascript\"><!--
\t\$('#button-confirm').bind('click', function () {

\t      if (\$('#eway-radio-cc').is(':checked')) {
\t        var eway_error = false;
\t        if (\$('#eway-cardname').val().length < 1) {
\t          eway_error = true;
\t          \$('#ewaycard-error').html('Card Holder\\'s Name must be entered');
\t        } else {
\t          \$('#ewaycard-error').empty();
\t        }

\t        var ccnum_regex = new RegExp(\"^[0-9]{13,19}\$\");
\t        if (!ccnum_regex.test(\$('#eway-cardnumber').val().replace(/ /g, '')) || !luhn10(\$('#eway-cardnumber').val())) {
\t          eway_error = true;
\t          \$('#ewaynumber-error').html('Card Number appears invalid');
\t        } else {
\t          \$('#ewaynumber-error').empty();
\t        }

\t        var cc_year = parseInt(\$('#eway-card-expiry-year').val(), 10);
\t        var cc_month = parseInt(\$('#eway-card-expiry-month').val(), 10);

\t        var cc_expiry = new Date(cc_year, cc_month, 1);
\t        var cc_expired = new Date(cc_expiry - 1);
\t        var today = new Date();

\t        if (today.getTime() > cc_expired.getTime()) {
\t          eway_error = true;
\t          \$('#expiry-error').html('This expiry date has passed');
\t        } else {
\t          \$('#expiry-error').empty();
\t        }

\t        var ccv_regex = new RegExp(\"^[0-9]{3,4}\$\");
\t        if (!ccv_regex.test(\$('#eway-cardcvn').val().replace(/ /g, ''))) {
\t          eway_error = true;
\t          \$('#ewaycvn-error').html('CVV/CSV Number appears invalid');
\t        } else {
\t          \$('#ewaycvn-error').empty();
\t        }

\t        if (eway_error) {
\t          return false;
\t        }
\t      }

\t      \$('#eway-payment-form').submit();
\t      \$('#button-confirm').button('loading');
\t      \$(\"#button-confirm\").prop('disabled', true);

\t    });

\t    var luhn10 = function (a, b, c, d, e) {
\t      for (d = +a[b = a.length - 1], e = 0; b--; ) {
\t        c = +a[b], d += ++e % 2 ? 2 * c % 10 + (c > 4) : c;
\t      }
\t      return !(d % 10)
\t    };

\t//--></script> 
";
        }
    }

    public function getTemplateName()
    {
        return "default/template/extension/payment/eway.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  274 => 111,  268 => 107,  262 => 103,  257 => 102,  253 => 101,  249 => 100,  245 => 99,  240 => 97,  231 => 90,  219 => 86,  214 => 83,  210 => 82,  199 => 73,  187 => 69,  182 => 66,  178 => 65,  169 => 59,  162 => 55,  157 => 53,  150 => 49,  145 => 47,  140 => 44,  138 => 43,  135 => 42,  128 => 40,  125 => 39,  118 => 37,  116 => 36,  111 => 33,  106 => 31,  102 => 29,  99 => 28,  94 => 26,  90 => 24,  87 => 23,  79 => 21,  74 => 20,  69 => 19,  64 => 18,  60 => 17,  56 => 15,  54 => 14,  49 => 11,  43 => 9,  41 => 8,  37 => 7,  33 => 6,  27 => 4,  21 => 2,  19 => 1,);
    }
}
/* {% if error %}*/
/* <div class="alert alert-danger alert-dismissible">Payment Error: {{ error }}</div>*/
/* {% else %}*/
/* <form action="{{ action }}" method="POST" class="form-horizontal" id="eway-payment-form">*/
/*   <fieldset id="payment">*/
/*     <legend>{{ text_credit_card }}</legend>*/
/*     <input type="hidden" name="EWAY_ACCESSCODE" value="{{ AccessCode }}" />*/
/*     {% if text_testing %}*/
/*     <div class="alert alert-warning alert-dismissible">{{ text_testing }}</div>*/
/*     {% endif %}*/
/*     <div class="form-group">*/
/*       <div class="col-sm-12">*/
/*         <ul>*/
/*           {% if payment_type.visa == 1 or payment_type.mastercard == 1 or payment_type.diners == 1 or payment_type.jcb == 1 or payment_type.amex == 1 %}*/
/*           <label>*/
/*             <input type="radio" name="EWAY_PAYMENTTYPE" id="eway-radio-cc" value="creditcard" checked="checked" onchange="javascript:select_eWAYPaymentOption('creditcard')" />*/
/*             {% if payment_type.visa == 1 %} <img src="catalog/view/theme/default/image/eway_creditcard_visa.png" height="30" alt="Visa" /> {% endif %}*/
/*             {% if payment_type.mastercard == 1 %} <img src="catalog/view/theme/default/image/eway_creditcard_master.png" height="30" alt="MasterCard" /> {% endif %}*/
/*             {% if payment_type.diners == 1 %} <img src="catalog/view/theme/default/image/eway_creditcard_diners.png" height="30" alt="Diners Club" /> {% endif %}*/
/*             {% if payment_type.jcb == 1 %} <img src="catalog/view/theme/default/image/eway_creditcard_jcb.png" height="30" alt="JCB" /> {% endif %}*/
/*             {% if payment_type.amex == 1 %} <img src="catalog/view/theme/default/image/eway_creditcard_amex.png" height="30" alt="AMEX" /> {% endif %} </label>*/
/*           {% endif %}*/
/*           {% if payment_type.paypal == 1 %}*/
/*           <label>*/
/*             <input type="radio" name="EWAY_PAYMENTTYPE" value="paypal" onchange="javascript:select_eWAYPaymentOption(paypal)" />*/
/*             <img src="catalog/view/theme/default/image/eway_paypal.png" height="30" alt="{{ text_card_type_pp }}" /></label>*/
/*           {% endif %}*/
/*           {% if payment_type.masterpass == 1 %}*/
/*           <label>*/
/*             <input type="radio" name="EWAY_PAYMENTTYPE" value="masterpass" onchange="javascript:select_eWAYPaymentOption(masterpass)" />*/
/*             <img src="catalog/view/theme/default/image/eway_masterpass.png" height="30" alt="{{ text_card_type_mp }}" /></label>*/
/*           {% endif %}*/
/*         </ul>*/
/*       </div>*/
/*     </div>*/
/*     {% if payment_type.paypal == 1 %}*/
/*     <p id="tip-paypal" style="display:none;">{{ text_type_help }}{{ text_card_type_pp }}</p>*/
/*     {% endif %}*/
/*     {% if payment_type.masterpass == 1 %}*/
/*     <p id="tip-masterpass" style="display:none;">{{ text_type_help }}{{ text_card_type_mp }}</p>*/
/*     {% endif %}*/
/*     */
/*         {% if payment_type.visa == 1 or payment_type.mastercard == 1 or payment_type.diners == 1 or payment_type.jcb == 1 or payment_type.amex == 1 %}*/
/*       */
/*     <div id="creditcard-info">*/
/*     <div class="form-group required">*/
/*       <label class="col-sm-2 control-label" for="eway-cardname">{{ entry_cc_name }}</label>*/
/*       <div class="col-sm-10">*/
/*         <input name="EWAY_CARDNAME" type="text" value="" id="eway-cardname" placeholder="{{ entry_cc_name }}"  autocomplete="off" class="form-control"/>*/
/*         <span id="ewaycard-error" class="text-danger"></span> </div>*/
/*     </div>*/
/*     <div class="form-group required">*/
/*       <label class="col-sm-2 control-label" for="eway-cardnumber">{{ entry_cc_number }}</label>*/
/*       <div class="col-sm-10">*/
/*         <input name="EWAY_CARDNUMBER" type="text" maxlength="19" id="eway-cardnumber" value="" placeholder="{{ entry_cc_number }}"  autocomplete="off" class="form-control" pattern="\d*" />*/
/*         <span id="ewaynumber-error" class="text-danger"></span> </div>*/
/*     </div>*/
/*     <div class="form-group required">*/
/*       <label class="col-sm-2 control-label" for="eway-card-expiry-month">{{ entry_cc_expire_date }}</label>*/
/*       <div class="col-sm-2">*/
/*         <select name="EWAY_CARDEXPIRYMONTH" id="eway-card-expiry-month" class="form-control">*/
/*           */
/*           */
/* 					*/
/*         {% for month in months %}*/
/*       */
/* 						*/
/*           */
/*           <option value="{{ month.value }}">{{ month.text }}</option>*/
/*           */
/*           */
/* 					{% endfor %}*/
/* 				  */
/*         */
/*         </select>*/
/*       </div>*/
/*       <div class="col-sm-2">*/
/*         <select name="EWAY_CARDEXPIRYYEAR" id="eway-card-expiry-year" class="form-control">*/
/*           */
/*           */
/* 					*/
/*         {% for year in year_expire %}*/
/*       */
/* 						*/
/*           */
/*           <option value="{{ year.value }}">{{ year.text }}</option>*/
/*           */
/*           */
/* 					{% endfor %}*/
/* 				  */
/*         */
/*         </select>*/
/*         <div id="expiry-error" class="text-danger"></div>*/
/*       </div>*/
/*     </div>*/
/*     <div class="form-group required">*/
/*       <label class="col-sm-2 control-label" for="eway-cardcvn">{{ entry_cc_cvv2 }}</label>*/
/*       <div class="col-sm-10">*/
/*         <input name="EWAY_CARDCVN" type="text" maxlength="4" value="" placeholder="{{ entry_cc_cvv2 }}" id="eway-cardcvn" autocomplete="off" class="form-control" pattern="\d*" />*/
/*         <span id="cvn-details" class="help"> {{ help_cvv }}*/
/*         {% if 'amex' in payment_type %}<br />*/
/*         {{ help_cvv_amex }}*/
/*         {% endif %} </span><br />*/
/*         <span id="ewaycvn-error" class="text-danger"></span> </div>*/
/*     </div>*/
/*     {% endif %}*/
/*   </fieldset>*/
/* </form>*/
/* <div class="buttons">*/
/*   <div class="pull-right">*/
/*     <input type="button" value="{{ button_confirm }}" id="button-confirm" data-loading-text="{{ text_loading }}" class="btn btn-primary" />*/
/*   </div>*/
/* </div>*/
/* <script language="JavaScript" type="text/javascript" >//<!--*/
/* 	    function select_eWAYPaymentOption(v) {*/
/* 	      if ($("#creditcard-info").length) {*/
/* 	        $("#creditcard-info").hide();*/
/* 	      }*/
/* 	      if ($("#tip-paypal").length) {*/
/* 	        $("#tip-paypal").hide();*/
/* 	      }*/
/* 	      if ($("#tip-masterpass").length) {*/
/* 	        $("#tip-masterpass").hide();*/
/* 	      }*/
/* 	      if ($("#tip-vme").length) {*/
/* 	        $("#tip-vme").hide();*/
/* 	      }*/
/* 	      if (v == 'creditcard') {*/
/* 	        $("#creditcard-info").show();*/
/* 	      } else {*/
/* 	        $("#tip-" + v).show();*/
/* 	      }*/
/* 	    }*/
/* 	//--></script> */
/* <script type="text/javascript"><!--*/
/* 	$('#button-confirm').bind('click', function () {*/
/* */
/* 	      if ($('#eway-radio-cc').is(':checked')) {*/
/* 	        var eway_error = false;*/
/* 	        if ($('#eway-cardname').val().length < 1) {*/
/* 	          eway_error = true;*/
/* 	          $('#ewaycard-error').html('Card Holder\'s Name must be entered');*/
/* 	        } else {*/
/* 	          $('#ewaycard-error').empty();*/
/* 	        }*/
/* */
/* 	        var ccnum_regex = new RegExp("^[0-9]{13,19}$");*/
/* 	        if (!ccnum_regex.test($('#eway-cardnumber').val().replace(/ /g, '')) || !luhn10($('#eway-cardnumber').val())) {*/
/* 	          eway_error = true;*/
/* 	          $('#ewaynumber-error').html('Card Number appears invalid');*/
/* 	        } else {*/
/* 	          $('#ewaynumber-error').empty();*/
/* 	        }*/
/* */
/* 	        var cc_year = parseInt($('#eway-card-expiry-year').val(), 10);*/
/* 	        var cc_month = parseInt($('#eway-card-expiry-month').val(), 10);*/
/* */
/* 	        var cc_expiry = new Date(cc_year, cc_month, 1);*/
/* 	        var cc_expired = new Date(cc_expiry - 1);*/
/* 	        var today = new Date();*/
/* */
/* 	        if (today.getTime() > cc_expired.getTime()) {*/
/* 	          eway_error = true;*/
/* 	          $('#expiry-error').html('This expiry date has passed');*/
/* 	        } else {*/
/* 	          $('#expiry-error').empty();*/
/* 	        }*/
/* */
/* 	        var ccv_regex = new RegExp("^[0-9]{3,4}$");*/
/* 	        if (!ccv_regex.test($('#eway-cardcvn').val().replace(/ /g, ''))) {*/
/* 	          eway_error = true;*/
/* 	          $('#ewaycvn-error').html('CVV/CSV Number appears invalid');*/
/* 	        } else {*/
/* 	          $('#ewaycvn-error').empty();*/
/* 	        }*/
/* */
/* 	        if (eway_error) {*/
/* 	          return false;*/
/* 	        }*/
/* 	      }*/
/* */
/* 	      $('#eway-payment-form').submit();*/
/* 	      $('#button-confirm').button('loading');*/
/* 	      $("#button-confirm").prop('disabled', true);*/
/* */
/* 	    });*/
/* */
/* 	    var luhn10 = function (a, b, c, d, e) {*/
/* 	      for (d = +a[b = a.length - 1], e = 0; b--; ) {*/
/* 	        c = +a[b], d += ++e % 2 ? 2 * c % 10 + (c > 4) : c;*/
/* 	      }*/
/* 	      return !(d % 10)*/
/* 	    };*/
/* */
/* 	//--></script> */
/* {% endif %}*/
