<?php

/* extension/module/excelport/tab_support.twig */
class __TwigTemplate_dc0b66caee4d0ce988e6dc54664feb56b81b893f7f4f57aef6bae060cfcd98e9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"row\">
    <div class=\"col-md-4\">
        <div class=\"panel\">
            <div class=\"panel-heading\">
                <h3 class=\"panel-title\"><i class=\"fa fa-user\"></i> ";
        // line 5
        echo (isset($context["license_your_license"]) ? $context["license_your_license"] : null);
        echo "</h3>
            </div>
            <div class=\"panel-body\">
            ";
        // line 8
        if ( !$this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "ExcelPort", array()), "LicensedOn", array())) {
            // line 9
            echo "                <div class=\"form-group\">
                    <label for=\"moduleLicense\">";
            // line 10
            echo (isset($context["license_enter_code"]) ? $context["license_enter_code"] : null);
            echo "</label>
                    <div class=\"licenseAlerts\"></div>
                    <div class=\"licenseDiv\"></div>
                    <input type=\"text\" class=\"licenseCodeBox form-control\" placeholder=\"";
            // line 13
            echo (isset($context["license_placeholder"]) ? $context["license_placeholder"] : null);
            echo "\" value=\"";
            echo (((array_key_exists("license_code", $context) &&  !(null === (isset($context["license_code"]) ? $context["license_code"] : null)))) ? ((isset($context["license_code"]) ? $context["license_code"] : null)) : (""));
            echo "\" />
                </div>

                <div class=\"form-group\">
                    <button type=\"button\" class=\"btn btn-success btnActivateLicense\"><i class=\"icon-ok\"></i> ";
            // line 17
            echo (isset($context["license_activate"]) ? $context["license_activate"] : null);
            echo "</button>
                    <div class=\"pull-right\">
                        <button type=\"button\" class=\"btn btn-link small-link\" onclick=\"window.open('http://isenselabs.com/users/purchases/')\">";
            // line 19
            echo (isset($context["license_get_code"]) ? $context["license_get_code"] : null);
            echo " <i class=\"fa fa-external-link\"></i></button>
                    </div>
                </div>
                <script type=\"text/javascript\">
                    var domainraw = location.protocol + '//' + location.host;
                    var domain = btoa(domainraw);
                    var timenow = ";
            // line 25
            echo (isset($context["now"]) ? $context["now"] : null);
            echo ";
                    var MID = 'TBY2PJCCI7';
                </script>
                <script type=\"text/javascript\" src=\"//isenselabs.com/external/validate/\"></script>
            ";
        }
        // line 30
        echo "
            ";
        // line 31
        if ($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "ExcelPort", array()), "LicensedOn", array())) {
            // line 32
            echo "                <input name=\"cHRpbWl6YXRpb24ef4fe\" type=\"hidden\" value=\"";
            echo (isset($context["licenseEncoded"]) ? $context["licenseEncoded"] : null);
            echo "\" />
                <input name=\"OaXRyb1BhY2sgLSBDb21\" type=\"hidden\" value=\"";
            // line 33
            echo $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "ExcelPort", array()), "LicensedOn", array());
            echo "\" />

                <div class=\"row\">
                    <label class=\"license_label\">";
            // line 36
            echo (isset($context["license_holder"]) ? $context["license_holder"] : null);
            echo "</label>
                    <span class=\"license_info\">";
            // line 37
            echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "ExcelPort", array()), "License", array()), "customerName", array());
            echo "</span>
                </div>
                <div class=\"row\">
                    <label class=\"license_label\">";
            // line 40
            echo (isset($context["license_registered_domains"]) ? $context["license_registered_domains"] : null);
            echo "</label>
                    <ul class=\"license_info\">
                        ";
            // line 42
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "ExcelPort", array()), "License", array()), "licenseDomainsUsed", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["domain"]) {
                // line 43
                echo "                            <li><i class=\"fa fa-check\"></i>&nbsp;";
                echo $context["domain"];
                echo "</li>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['domain'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 45
            echo "                    </ul>
                </div>
                <div class=\"row\">
                    <label class=\"license_label\">";
            // line 48
            echo (isset($context["license_expires"]) ? $context["license_expires"] : null);
            echo "</label>
                    <span class=\"license_info\">";
            // line 49
            echo (isset($context["expiration_date"]) ? $context["expiration_date"] : null);
            echo "</span>

                    <div class=\"alert alert-success center\">";
            // line 51
            echo (isset($context["license_valid"]) ? $context["license_valid"] : null);
            echo " [<a href=\"http://isenselabs.com/users/purchases\" target=\"_blank\">";
            echo (isset($context["license_manage"]) ? $context["license_manage"] : null);
            echo "</a>]</div>
                </div>
            ";
        }
        // line 54
        echo "            </div>
        </div>
    </div>

    <div class=\"col-md-8\">
        <div class=\"panel\">
            <div class=\"panel-heading\">
                <h3 class=\"panel-title\"><i class=\"fa fa-users\"></i> ";
        // line 61
        echo (isset($context["license_get_support"]) ? $context["license_get_support"] : null);
        echo "</h3>
            </div>
            <div class=\"panel-body\">
                <div class=\"row\">
                    <div class=\"col-md-4\">
                        <div class=\"thumbnail\">
                            <img alt=\"";
        // line 67
        echo (isset($context["license_community"]) ? $context["license_community"] : null);
        echo "\" src=\"view/image/excelport/community.png\">
                            <div class=\"caption center\">
                                <h3>";
        // line 69
        echo (isset($context["license_community"]) ? $context["license_community"] : null);
        echo "</h3>
                                <p>";
        // line 70
        echo (isset($context["license_community_info"]) ? $context["license_community_info"] : null);
        echo "</p>
                                <p style=\"padding-top: 5px;\"><a href=\"http://isenselabs.com/forum\" target=\"_blank\" class=\"btn btn-lg btn-default\">";
        // line 71
        echo (isset($context["license_forums"]) ? $context["license_forums"] : null);
        echo "</a></p>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-md-4\">
                        <div class=\"thumbnail\">
                            <img data-src=\"holder.js/300x200\" alt=\"Ticket support\" src=\"view/image/excelport/tickets.png\">
                            <div class=\"caption center\">
                                <h3>";
        // line 79
        echo (isset($context["license_tickets"]) ? $context["license_tickets"] : null);
        echo "</h3>
                                <p>";
        // line 80
        echo (isset($context["license_tickets_info"]) ? $context["license_tickets_info"] : null);
        echo "</p>
                                <p style=\"padding-top: 5px;\"><a href=\"http://isenselabs.com/tickets/open/";
        // line 81
        echo (isset($context["support_path"]) ? $context["support_path"] : null);
        echo "\" target=\"_blank\" class=\"btn btn-lg btn-default\">";
        echo (isset($context["license_tickets_open"]) ? $context["license_tickets_open"] : null);
        echo "</a></p>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-md-4\">
                        <div class=\"thumbnail\">
                            <img alt=\"";
        // line 87
        echo (isset($context["license_presale"]) ? $context["license_presale"] : null);
        echo "\" src=\"view/image/excelport/pre-sale.png\">
                            <div class=\"caption center\">
                                <h3>";
        // line 89
        echo (isset($context["license_presale"]) ? $context["license_presale"] : null);
        echo "</h3>
                                <p>";
        // line 90
        echo (isset($context["license_presale_info"]) ? $context["license_presale_info"] : null);
        echo "</p>
                                <p style=\"padding-top: 5px;\"><a href=\"https://isenselabs.com/pages/premium-services\" target=\"_blank\" class=\"btn btn-lg btn-default\">";
        // line 91
        echo (isset($context["license_presale_bump"]) ? $context["license_presale_bump"] : null);
        echo "</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "extension/module/excelport/tab_support.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  213 => 91,  209 => 90,  205 => 89,  200 => 87,  189 => 81,  185 => 80,  181 => 79,  170 => 71,  166 => 70,  162 => 69,  157 => 67,  148 => 61,  139 => 54,  131 => 51,  126 => 49,  122 => 48,  117 => 45,  108 => 43,  104 => 42,  99 => 40,  93 => 37,  89 => 36,  83 => 33,  78 => 32,  76 => 31,  73 => 30,  65 => 25,  56 => 19,  51 => 17,  42 => 13,  36 => 10,  33 => 9,  31 => 8,  25 => 5,  19 => 1,);
    }
}
/* <div class="row">*/
/*     <div class="col-md-4">*/
/*         <div class="panel">*/
/*             <div class="panel-heading">*/
/*                 <h3 class="panel-title"><i class="fa fa-user"></i> {{ license_your_license }}</h3>*/
/*             </div>*/
/*             <div class="panel-body">*/
/*             {% if not data.ExcelPort.LicensedOn %}*/
/*                 <div class="form-group">*/
/*                     <label for="moduleLicense">{{ license_enter_code }}</label>*/
/*                     <div class="licenseAlerts"></div>*/
/*                     <div class="licenseDiv"></div>*/
/*                     <input type="text" class="licenseCodeBox form-control" placeholder="{{ license_placeholder }}" value="{{ license_code ?? '' }}" />*/
/*                 </div>*/
/* */
/*                 <div class="form-group">*/
/*                     <button type="button" class="btn btn-success btnActivateLicense"><i class="icon-ok"></i> {{ license_activate }}</button>*/
/*                     <div class="pull-right">*/
/*                         <button type="button" class="btn btn-link small-link" onclick="window.open('http://isenselabs.com/users/purchases/')">{{ license_get_code }} <i class="fa fa-external-link"></i></button>*/
/*                     </div>*/
/*                 </div>*/
/*                 <script type="text/javascript">*/
/*                     var domainraw = location.protocol + '//' + location.host;*/
/*                     var domain = btoa(domainraw);*/
/*                     var timenow = {{ now }};*/
/*                     var MID = 'TBY2PJCCI7';*/
/*                 </script>*/
/*                 <script type="text/javascript" src="//isenselabs.com/external/validate/"></script>*/
/*             {% endif %}*/
/* */
/*             {% if data.ExcelPort.LicensedOn %}*/
/*                 <input name="cHRpbWl6YXRpb24ef4fe" type="hidden" value="{{ licenseEncoded }}" />*/
/*                 <input name="OaXRyb1BhY2sgLSBDb21" type="hidden" value="{{ data.ExcelPort.LicensedOn }}" />*/
/* */
/*                 <div class="row">*/
/*                     <label class="license_label">{{ license_holder }}</label>*/
/*                     <span class="license_info">{{ data.ExcelPort.License.customerName }}</span>*/
/*                 </div>*/
/*                 <div class="row">*/
/*                     <label class="license_label">{{ license_registered_domains }}</label>*/
/*                     <ul class="license_info">*/
/*                         {% for domain in data.ExcelPort.License.licenseDomainsUsed %}*/
/*                             <li><i class="fa fa-check"></i>&nbsp;{{ domain }}</li>*/
/*                         {% endfor %}*/
/*                     </ul>*/
/*                 </div>*/
/*                 <div class="row">*/
/*                     <label class="license_label">{{ license_expires }}</label>*/
/*                     <span class="license_info">{{ expiration_date }}</span>*/
/* */
/*                     <div class="alert alert-success center">{{ license_valid }} [<a href="http://isenselabs.com/users/purchases" target="_blank">{{ license_manage }}</a>]</div>*/
/*                 </div>*/
/*             {% endif %}*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* */
/*     <div class="col-md-8">*/
/*         <div class="panel">*/
/*             <div class="panel-heading">*/
/*                 <h3 class="panel-title"><i class="fa fa-users"></i> {{ license_get_support }}</h3>*/
/*             </div>*/
/*             <div class="panel-body">*/
/*                 <div class="row">*/
/*                     <div class="col-md-4">*/
/*                         <div class="thumbnail">*/
/*                             <img alt="{{ license_community }}" src="view/image/excelport/community.png">*/
/*                             <div class="caption center">*/
/*                                 <h3>{{ license_community }}</h3>*/
/*                                 <p>{{ license_community_info }}</p>*/
/*                                 <p style="padding-top: 5px;"><a href="http://isenselabs.com/forum" target="_blank" class="btn btn-lg btn-default">{{ license_forums }}</a></p>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="col-md-4">*/
/*                         <div class="thumbnail">*/
/*                             <img data-src="holder.js/300x200" alt="Ticket support" src="view/image/excelport/tickets.png">*/
/*                             <div class="caption center">*/
/*                                 <h3>{{ license_tickets }}</h3>*/
/*                                 <p>{{ license_tickets_info }}</p>*/
/*                                 <p style="padding-top: 5px;"><a href="http://isenselabs.com/tickets/open/{{ support_path }}" target="_blank" class="btn btn-lg btn-default">{{ license_tickets_open }}</a></p>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="col-md-4">*/
/*                         <div class="thumbnail">*/
/*                             <img alt="{{ license_presale }}" src="view/image/excelport/pre-sale.png">*/
/*                             <div class="caption center">*/
/*                                 <h3>{{ license_presale }}</h3>*/
/*                                 <p>{{ license_presale_info }}</p>*/
/*                                 <p style="padding-top: 5px;"><a href="https://isenselabs.com/pages/premium-services" target="_blank" class="btn btn-lg btn-default">{{ license_presale_bump }}</a></p>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
