<?php

/* journal2/template/journal2/module/side_column_menu.twig */
class __TwigTemplate_f859c62ab940a9e6c54024ec2f06dc9d036bfda2184b4e0518a77fe796eb5ea6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 17
        echo "
<div id=\"flyout-";
        // line 18
        echo (isset($context["module"]) ? $context["module"] : null);
        echo "\" class=\"flyout-";
        echo (isset($context["module_id"]) ? $context["module_id"] : null);
        echo " box flyout-menu\">
  <div class=\"box-heading\">";
        // line 19
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</div>
  <div class=\"flyout\">
    <ul>
      ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["menu_items"]) ? $context["menu_items"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["menu_item"]) {
            // line 23
            echo "        ";
            $context["menu"] = ((($this->getAttribute($context["menu_item"], "type", array()) == "drop-down")) ? ((twig_length_filter($this->env, $this->getAttribute($context["menu_item"], "subcategories", array())) > 0)) : (true));
            // line 24
            echo "
        <li class=\"fly-";
            // line 25
            echo $this->getAttribute($context["menu_item"], "type", array());
            echo "\">
          ";
            // line 26
            if ($this->getAttribute($context["menu_item"], "href", array())) {
                // line 27
                echo "            <a href=\"";
                echo $this->getAttribute($context["menu_item"], "href", array());
                echo "\" ";
                echo $this->getAttribute($context["menu_item"], "target", array());
                echo ">";
                echo $this->getAttribute($context["menu_item"], "icon", array());
                echo " ";
                if ( !$this->getAttribute($context["menu_item"], "hide_text", array())) {
                    echo "<span class=\"main-menu-text\">";
                    echo $this->getAttribute($context["menu_item"], "name", array());
                    echo "</span>";
                }
                if ((isset($context["menu"]) ? $context["menu"] : null)) {
                    echo "<i class=\"menu-plus\"></i>";
                }
                echo "</a><span class=\"clear\"></span>
          ";
            } else {
                // line 29
                echo "            <a>";
                echo $this->getAttribute($context["menu_item"], "icon", array());
                if ( !$this->getAttribute($context["menu_item"], "hide_text", array())) {
                    echo "<span class=\"main-menu-text\">";
                    echo $this->getAttribute($context["menu_item"], "name", array());
                    echo "</span>";
                }
                echo " ";
                if ((isset($context["menu"]) ? $context["menu"] : null)) {
                    echo "<i class=\"menu-plus\"></i>";
                }
                echo "</a><span class=\"clear\"></span>
          ";
            }
            // line 31
            echo "
          ";
            // line 32
            if ((($this->getAttribute($context["menu_item"], "type", array()) == "mega-menu-mixed") && (twig_length_filter($this->env, $this->getAttribute($context["menu_item"], "mixed_columns", array())) > 0))) {
                // line 33
                echo "            <div class=\"fly-mega-menu mega-menu\" style=\"";
                echo $this->getAttribute($context["menu_item"], "css", array());
                echo "\">
              <div>
                ";
                // line 35
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["menu_item"], "mixed_columns", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["menu_column"]) {
                    // line 36
                    echo "                  ";
                    if ((($this->getAttribute($context["menu_column"], "type", array()) == "mega-menu-categories") && (twig_length_filter($this->env, $this->getAttribute($context["menu_column"], "items", array())) > 0))) {
                        // line 37
                        echo "                    <div class=\"fly-column mega-menu-column mega-menu-categories ";
                        echo $this->getAttribute($context["menu_column"], "class", array());
                        echo "\" style=\"width: ";
                        echo $this->getAttribute($context["menu_column"], "width", array());
                        echo "\">
                      ";
                        // line 38
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["menu_column"], "top_cms_blocks", array()));
                        foreach ($context['_seq'] as $context["_key"] => $context["cms_block"]) {
                            // line 39
                            echo "                        <div class=\"menu-cms-block\">";
                            echo $this->getAttribute($context["cms_block"], "content", array());
                            echo "</div>
                      ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cms_block'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 41
                        echo "                      <div>
                        ";
                        // line 42
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["menu_column"], "items", array()));
                        foreach ($context['_seq'] as $context["_key"] => $context["submenu_item"]) {
                            // line 43
                            echo "                          <div class=\"mega-menu-item ";
                            echo $this->getAttribute($context["menu_column"], "classes", array());
                            echo " ";
                            echo $this->getAttribute($context["menu_column"], "show_class", array());
                            echo "\">
                            <div>
                              <h3><a href=\"";
                            // line 45
                            echo $this->getAttribute($context["submenu_item"], "href", array());
                            echo "\">";
                            echo $this->getAttribute($context["submenu_item"], "name", array());
                            echo "</a></h3>
                              ";
                            // line 46
                            if (twig_in_filter($this->getAttribute($context["menu_column"], "show", array()), array(0 => "image", 1 => "both"))) {
                                // line 47
                                echo "                                <a href=\"";
                                echo $this->getAttribute($context["submenu_item"], "href", array());
                                echo "\"><img width=\"";
                                echo $this->getAttribute($context["submenu_item"], "image_width", array());
                                echo "\" height=\"";
                                echo $this->getAttribute($context["submenu_item"], "image_height", array());
                                echo "\" class=\"lazy\" src=\"";
                                echo $this->getAttribute($context["submenu_item"], "dummy", array());
                                echo "\" data-src=\"";
                                echo $this->getAttribute($context["submenu_item"], "image", array());
                                echo "\" data-default-src=\"";
                                echo $this->getAttribute($context["submenu_item"], "image", array());
                                echo "\" alt=\"";
                                echo $this->getAttribute($context["submenu_item"], "name", array());
                                echo "\"/></a>
                              ";
                            }
                            // line 49
                            echo "                              ";
                            if (twig_in_filter($this->getAttribute($context["menu_column"], "show", array()), array(0 => "links", 1 => "both"))) {
                                // line 50
                                echo "                                <ul>
                                  ";
                                // line 51
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["submenu_item"], "items", array()));
                                $context['loop'] = array(
                                  'parent' => $context['_parent'],
                                  'index0' => 0,
                                  'index'  => 1,
                                  'first'  => true,
                                );
                                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                                    $length = count($context['_seq']);
                                    $context['loop']['revindex0'] = $length - 1;
                                    $context['loop']['revindex'] = $length;
                                    $context['loop']['length'] = $length;
                                    $context['loop']['last'] = 1 === $length;
                                }
                                foreach ($context['_seq'] as $context["_key"] => $context["sub2menu_item"]) {
                                    // line 52
                                    echo "                                    ";
                                    if ((($this->getAttribute($context["menu_column"], "limit", array()) == 0) || (($this->getAttribute($context["menu_column"], "limit", array()) > 0) && ($this->getAttribute($context["menu_column"], "limit", array()) >= $this->getAttribute($context["loop"], "index", array()))))) {
                                        // line 53
                                        echo "                                      <li data-image=\"";
                                        echo $this->getAttribute($context["sub2menu_item"], "image", array());
                                        echo "\"><a href=\"";
                                        echo $this->getAttribute($context["sub2menu_item"], "href", array());
                                        echo "\">";
                                        echo $this->getAttribute($context["sub2menu_item"], "name", array());
                                        echo "</a></li>
                                    ";
                                    } elseif ((($this->getAttribute(                                    // line 54
$context["menu_column"], "limit", array()) > 0) && ($this->getAttribute($context["menu_column"], "limit", array()) == $this->getAttribute($context["loop"], "index", array())))) {
                                        // line 55
                                        echo "                                      <li class=\"view-more\"><a href=\"";
                                        echo $this->getAttribute($context["submenu_item"], "href", array());
                                        echo "\">";
                                        echo $this->getAttribute($this->getAttribute((isset($context["journal2"]) ? $context["journal2"] : null), "settings", array()), "get", array(0 => "view_more_text"), "method");
                                        echo "</a></li>
                                    ";
                                    }
                                    // line 57
                                    echo "                                  ";
                                    ++$context['loop']['index0'];
                                    ++$context['loop']['index'];
                                    $context['loop']['first'] = false;
                                    if (isset($context['loop']['length'])) {
                                        --$context['loop']['revindex0'];
                                        --$context['loop']['revindex'];
                                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                                    }
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sub2menu_item'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 58
                                echo "                                </ul>
                              ";
                            }
                            // line 60
                            echo "                              <span class=\"clearfix\"> </span>
                            </div>
                          </div>
                        ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['submenu_item'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 64
                        echo "                      </div>
                      ";
                        // line 65
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["menu_column"], "bottom_cms_blocks", array()));
                        foreach ($context['_seq'] as $context["_key"] => $context["cms_block"]) {
                            // line 66
                            echo "                        <div class=\"menu-cms-block\">";
                            echo $this->getAttribute($context["cms_block"], "content", array());
                            echo "</div>
                      ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cms_block'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 68
                        echo "                    </div>
                  ";
                    }
                    // line 70
                    echo "
                  ";
                    // line 71
                    if ((($this->getAttribute($context["menu_column"], "type", array()) == "mega-menu-products") && (twig_length_filter($this->env, $this->getAttribute($context["menu_column"], "items", array())) > 0))) {
                        // line 72
                        echo "                    <div class=\"fly-column mega-menu-column mega-menu-products ";
                        echo $this->getAttribute($context["menu_column"], "class", array());
                        echo "\" style=\"width: ";
                        echo $this->getAttribute($context["menu_column"], "width", array());
                        echo "\">
                      ";
                        // line 73
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["menu_column"], "top_cms_blocks", array()));
                        foreach ($context['_seq'] as $context["_key"] => $context["cms_block"]) {
                            // line 74
                            echo "                        <div class=\"menu-cms-block\">";
                            echo $this->getAttribute($context["cms_block"], "content", array());
                            echo "</div>
                      ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cms_block'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 76
                        echo "                      <div>
                        ";
                        // line 77
                        if ($this->getAttribute($context["menu_column"], "name", array())) {
                            // line 78
                            echo "                          <h3>";
                            echo $this->getAttribute($context["menu_column"], "name", array());
                            echo "</h3>
                        ";
                        }
                        // line 80
                        echo "                        ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["menu_column"], "items", array()));
                        $context['loop'] = array(
                          'parent' => $context['_parent'],
                          'index0' => 0,
                          'index'  => 1,
                          'first'  => true,
                        );
                        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                            $length = count($context['_seq']);
                            $context['loop']['revindex0'] = $length - 1;
                            $context['loop']['revindex'] = $length;
                            $context['loop']['length'] = $length;
                            $context['loop']['last'] = 1 === $length;
                        }
                        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                            // line 81
                            echo "                          ";
                            if ((($this->getAttribute($context["menu_column"], "limit", array()) == 0) || (($this->getAttribute($context["menu_column"], "limit", array()) > 0) && ($this->getAttribute($context["menu_column"], "limit", array()) >= $this->getAttribute($context["loop"], "index", array()))))) {
                                // line 82
                                echo "                            <div class=\"mega-menu-item product-grid-item ";
                                echo $this->getAttribute($context["menu_column"], "classes", array());
                                echo " display-";
                                echo $this->getAttribute($this->getAttribute((isset($context["journal2"]) ? $context["journal2"] : null), "settings", array()), "get", array(0 => "product_grid_wishlist_icon_display"), "method");
                                echo " ";
                                echo $this->getAttribute($this->getAttribute((isset($context["journal2"]) ? $context["journal2"] : null), "settings", array()), "get", array(0 => "product_grid_button_block_button"), "method");
                                echo "\">
                              <div class=\"product-wrapper ";
                                // line 83
                                if (($this->getAttribute($context["product"], "labels", array()) && $this->getAttribute($this->getAttribute($context["product"], "labels", array()), "outofstock", array()))) {
                                    echo " outofstock ";
                                }
                                echo "\">
                                <div class=\"image\">
                                  <a href=\"";
                                // line 85
                                echo $this->getAttribute($context["product"], "href", array());
                                echo "\">
                                    <img class=\"lazy\" src=\"";
                                // line 86
                                echo $this->getAttribute($context["product"], "dummy", array());
                                echo "\" data-src=\"";
                                echo $this->getAttribute($context["product"], "image", array());
                                echo "\" width=\"";
                                echo $this->getAttribute($context["product"], "image_width", array());
                                echo "\" height=\"";
                                echo $this->getAttribute($context["product"], "image_height", array());
                                echo "\" alt=\"";
                                echo $this->getAttribute($context["product"], "name", array());
                                echo "\"/>
                                  </a>
                                  ";
                                // line 88
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["product"], "labels", array()));
                                foreach ($context['_seq'] as $context["label"] => $context["name"]) {
                                    // line 89
                                    echo "                                    <span class=\"label-";
                                    echo $context["label"];
                                    echo "\"><b>";
                                    echo $context["name"];
                                    echo "</b></span>
                                  ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['label'], $context['name'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 91
                                echo "                                  ";
                                if ((($this->getAttribute($this->getAttribute((isset($context["journal2"]) ? $context["journal2"] : null), "settings", array()), "get", array(0 => "product_grid_wishlist_icon_position"), "method") == "image") && ($this->getAttribute($this->getAttribute((isset($context["journal2"]) ? $context["journal2"] : null), "settings", array()), "get", array(0 => "product_grid_wishlist_icon_display"), "method") == "icon"))) {
                                    // line 92
                                    echo "                                    <div class=\"wishlist\"><a onclick=\"addToWishList('";
                                    echo $this->getAttribute($context["product"], "product_id", array());
                                    echo "');\" class=\"hint--top\" data-hint=\"";
                                    echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
                                    echo "\"><i class=\"wishlist-icon\"></i><span class=\"button-wishlist-text\">";
                                    echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
                                    echo "</span></a></div>
                                    <div class=\"compare\"><a onclick=\"addToCompare('";
                                    // line 93
                                    echo $this->getAttribute($context["product"], "product_id", array());
                                    echo "');\" class=\"hint--top\" data-hint=\"";
                                    echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
                                    echo "\"><i class=\"compare-icon\"></i><span class=\"button-compare-text\">";
                                    echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
                                    echo "</span></a></div>
                                  ";
                                }
                                // line 95
                                echo "                                </div>
                                <div class=\"product-details\">
                                  <div class=\"caption\">
                                    <div class=\"name\"><a href=\"";
                                // line 98
                                echo $this->getAttribute($context["product"], "href", array());
                                echo "\">";
                                echo $this->getAttribute($context["product"], "name", array());
                                echo "</a></div>
                                    ";
                                // line 99
                                if ($this->getAttribute($context["product"], "price", array())) {
                                    // line 100
                                    echo "                                      <div class=\"price\">
                                        ";
                                    // line 101
                                    if ( !$this->getAttribute($context["product"], "special", array())) {
                                        // line 102
                                        echo "                                          ";
                                        echo $this->getAttribute($context["product"], "price", array());
                                        echo "
                                        ";
                                    } else {
                                        // line 104
                                        echo "                                          <span class=\"price-old\">";
                                        echo $this->getAttribute($context["product"], "price", array());
                                        echo "</span> <span class=\"price-new\">";
                                        echo $this->getAttribute($context["product"], "special", array());
                                        echo "</span>
                                        ";
                                    }
                                    // line 106
                                    echo "                                      </div>
                                    ";
                                }
                                // line 108
                                echo "                                    ";
                                if ($this->getAttribute($context["product"], "rating", array())) {
                                    // line 109
                                    echo "                                      <div class=\"rating\">
                                        ";
                                    // line 110
                                    $context['_parent'] = $context;
                                    $context['_seq'] = twig_ensure_traversable(range(1, 5));
                                    foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                                        // line 111
                                        echo "                                          ";
                                        if (($this->getAttribute($context["product"], "rating", array()) < $context["i"])) {
                                            // line 112
                                            echo "                                            <span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
                                          ";
                                        } else {
                                            // line 114
                                            echo "                                            <span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-2x\"></i><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
                                          ";
                                        }
                                        // line 116
                                        echo "                                        ";
                                    }
                                    $_parent = $context['_parent'];
                                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                                    $context = array_intersect_key($context, $_parent) + $_parent;
                                    // line 117
                                    echo "                                      </div>
                                    ";
                                }
                                // line 119
                                echo "                                  </div>
                                  <div class=\"button-group\">
                                    ";
                                // line 121
                                if (call_user_func_array($this->env->getFunction('staticCall')->getCallable(), array("Journal2Utils", "isEnquiryProduct", array(0 => null, 1 => $context["product"])))) {
                                    // line 122
                                    echo "                                      <div class=\"cart enquiry-button\">
                                        <a href=\"javascript:Journal.openPopup('";
                                    // line 123
                                    echo $this->getAttribute($this->getAttribute((isset($context["journal2"]) ? $context["journal2"] : null), "settings", array()), "get", array(0 => "enquiry_popup_code"), "method");
                                    echo "', '";
                                    echo $this->getAttribute($context["product"], "product_id", array());
                                    echo "');\" data-clk=\"addToCart('";
                                    echo $this->getAttribute($context["product"], "product_id", array());
                                    echo "');\" class=\"button hint--top\" data-hint=\"";
                                    echo $this->getAttribute($this->getAttribute((isset($context["journal2"]) ? $context["journal2"] : null), "settings", array()), "get", array(0 => "enquiry_button_text"), "method");
                                    echo "\">";
                                    echo $this->getAttribute($this->getAttribute((isset($context["journal2"]) ? $context["journal2"] : null), "settings", array()), "get", array(0 => "enquiry_button_icon"), "method");
                                    echo "<span class=\"button-cart-text\">";
                                    echo $this->getAttribute($this->getAttribute((isset($context["journal2"]) ? $context["journal2"] : null), "settings", array()), "get", array(0 => "enquiry_button_text"), "method");
                                    echo "</span></a>
                                      </div>
                                    ";
                                } else {
                                    // line 126
                                    echo "                                      <div class=\"cart ";
                                    if (($this->getAttribute($context["product"], "labels", array()) && $this->getAttribute($this->getAttribute($context["product"], "labels", array()), "outofstock", array()))) {
                                        echo " outofstock ";
                                    }
                                    echo "\">
                                        <a onclick=\"addToCart('";
                                    // line 127
                                    echo $this->getAttribute($context["product"], "product_id", array());
                                    echo "', '";
                                    echo $this->getAttribute($context["product"], "minimum", array());
                                    echo "');\" class=\"button hint--top\" data-hint=\"";
                                    echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                                    echo "\"><i class=\"button-left-icon\"></i><span class=\"button-cart-text\">";
                                    echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                                    echo "</span><i class=\"button-right-icon\"></i></a>
                                      </div>
                                    ";
                                }
                                // line 130
                                echo "                                    <div class=\"wishlist\"><a onclick=\"addToWishList('";
                                echo $this->getAttribute($context["product"], "product_id", array());
                                echo "');\" class=\"hint--top\" data-hint=\"";
                                echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
                                echo "\"><i class=\"wishlist-icon\"></i><span class=\"button-wishlist-text\">";
                                echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
                                echo "</span></a></div>
                                    <div class=\"compare\"><a onclick=\"addToCompare('";
                                // line 131
                                echo $this->getAttribute($context["product"], "product_id", array());
                                echo "');\" class=\"hint--top\" data-hint=\"";
                                echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
                                echo "\"><i class=\"compare-icon\"></i><span class=\"button-compare-text\">";
                                echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
                                echo "</span></a></div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          ";
                            }
                            // line 137
                            echo "                        ";
                            ++$context['loop']['index0'];
                            ++$context['loop']['index'];
                            $context['loop']['first'] = false;
                            if (isset($context['loop']['length'])) {
                                --$context['loop']['revindex0'];
                                --$context['loop']['revindex'];
                                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                            }
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 138
                        echo "                      </div>
                      ";
                        // line 139
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["menu_column"], "bottom_cms_blocks", array()));
                        foreach ($context['_seq'] as $context["_key"] => $context["cms_block"]) {
                            // line 140
                            echo "                        <div class=\"menu-cms-block\">";
                            echo $this->getAttribute($context["cms_block"], "content", array());
                            echo "</div>
                      ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cms_block'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 142
                        echo "                    </div>
                  ";
                    }
                    // line 144
                    echo "
                  ";
                    // line 145
                    if ((($this->getAttribute($context["menu_column"], "type", array()) == "mega-menu-brands") && (twig_length_filter($this->env, $this->getAttribute($context["menu_column"], "items", array())) > 0))) {
                        // line 146
                        echo "                    <div class=\"fly-column mega-menu-column mega-menu-brands ";
                        echo $this->getAttribute($context["menu_column"], "class", array());
                        echo "\" style=\"width: ";
                        echo $this->getAttribute($context["menu_column"], "width", array());
                        echo "\">
                      ";
                        // line 147
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["menu_column"], "top_cms_blocks", array()));
                        foreach ($context['_seq'] as $context["_key"] => $context["cms_block"]) {
                            // line 148
                            echo "                        <div class=\"menu-cms-block\">";
                            echo $this->getAttribute($context["cms_block"], "content", array());
                            echo "</div>
                      ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cms_block'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 150
                        echo "                      <div>
                        ";
                        // line 151
                        if ($this->getAttribute($context["menu_column"], "name", array())) {
                            // line 152
                            echo "                          <h3>";
                            echo $this->getAttribute($context["menu_column"], "name", array());
                            echo "</h3>
                        ";
                        }
                        // line 154
                        echo "                        ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["menu_column"], "items", array()));
                        foreach ($context['_seq'] as $context["_key"] => $context["submenu_item"]) {
                            // line 155
                            echo "                          <div class=\"mega-menu-item ";
                            echo $this->getAttribute($context["menu_column"], "classes", array());
                            echo "\">
                            <div>
                              ";
                            // line 157
                            if ((($this->getAttribute($context["submenu_item"], "show", array()) != "image") && $this->getAttribute($context["submenu_item"], "name", array()))) {
                                // line 158
                                echo "                                <h3><a href=\"";
                                echo $this->getAttribute($context["submenu_item"], "href", array());
                                echo "\">";
                                echo $this->getAttribute($context["submenu_item"], "name", array());
                                echo "</a></h3>
                              ";
                            }
                            // line 160
                            echo "                              <div>
                                ";
                            // line 161
                            if (($this->getAttribute($context["submenu_item"], "show", array()) != "text")) {
                                // line 162
                                echo "                                  <a href=\"";
                                echo $this->getAttribute($context["submenu_item"], "href", array());
                                echo "\"> <img width=\"";
                                echo $this->getAttribute($context["submenu_item"], "image_width", array());
                                echo "\" height=\"";
                                echo $this->getAttribute($context["submenu_item"], "image_height", array());
                                echo "\" class=\"lazy\" src=\"";
                                echo $this->getAttribute($context["submenu_item"], "dummy", array());
                                echo "\" data-src=\"";
                                echo $this->getAttribute($context["submenu_item"], "image", array());
                                echo "\" alt=\"";
                                echo $this->getAttribute($context["submenu_item"], "name", array());
                                echo "\"/></a>
                                ";
                            }
                            // line 164
                            echo "                                <ul>
                                  ";
                            // line 165
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["submenu_item"], "items", array()));
                            foreach ($context['_seq'] as $context["_key"] => $context["sub2menu_item"]) {
                                // line 166
                                echo "                                    <li data-image=\"";
                                echo $this->getAttribute($context["sub2menu_item"], "image", array());
                                echo "\"><a href=\"";
                                echo $this->getAttribute($context["sub2menu_item"], "href", array());
                                echo "\">";
                                echo $this->getAttribute($context["sub2menu_item"], "name", array());
                                echo "</a></li>
                                  ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sub2menu_item'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 168
                            echo "                                </ul>
                              </div>
                              <span class=\"clearfix\"></span>
                            </div>
                          </div>
                        ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['submenu_item'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 174
                        echo "                      </div>
                      ";
                        // line 175
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["menu_column"], "bottom_cms_blocks", array()));
                        foreach ($context['_seq'] as $context["_key"] => $context["cms_block"]) {
                            // line 176
                            echo "                        <div class=\"menu-cms-block\">";
                            echo $this->getAttribute($context["cms_block"], "content", array());
                            echo "</div>
                      ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cms_block'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 178
                        echo "                    </div>
                  ";
                    }
                    // line 180
                    echo "
                  ";
                    // line 181
                    if (($this->getAttribute($context["menu_column"], "type", array()) == "mega-menu-html-block")) {
                        // line 182
                        echo "                    <div class=\"fly-column mega-menu-column mega-menu-html mega-menu-html-block ";
                        echo $this->getAttribute($context["menu_column"], "class", array());
                        echo "\" style=\"width: ";
                        echo $this->getAttribute($context["menu_column"], "width", array());
                        echo "\">
                      ";
                        // line 183
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["menu_column"], "top_cms_blocks", array()));
                        foreach ($context['_seq'] as $context["_key"] => $context["cms_block"]) {
                            // line 184
                            echo "                        <div class=\"menu-cms-block\">";
                            echo $this->getAttribute($context["cms_block"], "content", array());
                            echo "</div>
                      ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cms_block'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 186
                        echo "                      <div>
                        ";
                        // line 187
                        if ($this->getAttribute($context["menu_column"], "name", array())) {
                            // line 188
                            echo "                          <h3>";
                            echo $this->getAttribute($context["menu_column"], "name", array());
                            echo "</h3>
                        ";
                        }
                        // line 190
                        echo "                        <div class=\"wrapper\">
                          ";
                        // line 191
                        echo $this->getAttribute($context["menu_column"], "html_text", array());
                        echo "
                        </div>
                      </div>
                      ";
                        // line 194
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["menu_column"], "bottom_cms_blocks", array()));
                        foreach ($context['_seq'] as $context["_key"] => $context["cms_block"]) {
                            // line 195
                            echo "                        <div class=\"menu-cms-block\">";
                            echo $this->getAttribute($context["cms_block"], "content", array());
                            echo "</div>
                      ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cms_block'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 197
                        echo "                    </div>
                  ";
                    }
                    // line 199
                    echo "                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menu_column'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 200
                echo "              </div>
            </div>
          ";
            }
            // line 203
            echo "
          ";
            // line 204
            if ((($this->getAttribute($context["menu_item"], "type", array()) == "drop-down") && (twig_length_filter($this->env, $this->getAttribute($context["menu_item"], "subcategories", array())) > 0))) {
                // line 205
                echo "            <ul>
              ";
                // line 206
                echo $this->getAttribute($this, "renderMultiLevelMenu", array(0 => $context["menu_item"]), "method");
                echo "
            </ul>
          ";
            }
            // line 209
            echo "        </li>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menu_item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 211
        echo "    </ul>
  </div>
  <script>
    \$(window).resize(function () {
      \$('#flyout-";
        // line 215
        echo (isset($context["module"]) ? $context["module"] : null);
        echo " .fly-mega-menu').css('max-width', \$(window).width() - 260);
    });
  </script>
</div>
";
    }

    // line 1
    public function getrenderMultiLevelMenu($__menu__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "menu" => $__menu__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 2
            echo "  ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "subcategories", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 3
                echo "    ";
                $context["submenu"] = $this->getAttribute($this, "renderMultiLevelMenu", array(0 => $context["item"]), "method");
                // line 4
                echo "    <li>
      <a ";
                // line 5
                if ($this->getAttribute($context["item"], "href", array())) {
                    echo " href=\"";
                    echo $this->getAttribute($context["item"], "href", array());
                    echo "\" ";
                    if ($this->getAttribute($context["item"], "target", array())) {
                        echo " ";
                        echo $this->getAttribute($context["item"], "target", array());
                        echo " ";
                    }
                    echo " ";
                }
                echo " class=\"";
                echo $this->getAttribute($context["item"], "class", array());
                echo "\">
        ";
                // line 6
                echo $this->getAttribute($context["item"], "name", array());
                echo "
        ";
                // line 7
                if ((twig_length_filter($this->env, $this->getAttribute($context["item"], "subcategories", array())) > 0)) {
                    // line 8
                    echo "          <i class=\"menu-plus\"></i>
        ";
                }
                // line 10
                echo "      </a>
      ";
                // line 11
                if ((twig_length_filter($this->env, $this->getAttribute($context["item"], "subcategories", array())) > 0)) {
                    // line 12
                    echo "        <ul> ";
                    echo (isset($context["submenu"]) ? $context["submenu"] : null);
                    echo "</ul>
      ";
                }
                // line 14
                echo "    </li>
  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        } catch (Throwable $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "journal2/template/journal2/module/side_column_menu.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  846 => 14,  840 => 12,  838 => 11,  835 => 10,  831 => 8,  829 => 7,  825 => 6,  809 => 5,  806 => 4,  803 => 3,  798 => 2,  786 => 1,  777 => 215,  771 => 211,  764 => 209,  758 => 206,  755 => 205,  753 => 204,  750 => 203,  745 => 200,  739 => 199,  735 => 197,  726 => 195,  722 => 194,  716 => 191,  713 => 190,  707 => 188,  705 => 187,  702 => 186,  693 => 184,  689 => 183,  682 => 182,  680 => 181,  677 => 180,  673 => 178,  664 => 176,  660 => 175,  657 => 174,  646 => 168,  633 => 166,  629 => 165,  626 => 164,  610 => 162,  608 => 161,  605 => 160,  597 => 158,  595 => 157,  589 => 155,  584 => 154,  578 => 152,  576 => 151,  573 => 150,  564 => 148,  560 => 147,  553 => 146,  551 => 145,  548 => 144,  544 => 142,  535 => 140,  531 => 139,  528 => 138,  514 => 137,  501 => 131,  492 => 130,  480 => 127,  473 => 126,  457 => 123,  454 => 122,  452 => 121,  448 => 119,  444 => 117,  438 => 116,  434 => 114,  430 => 112,  427 => 111,  423 => 110,  420 => 109,  417 => 108,  413 => 106,  405 => 104,  399 => 102,  397 => 101,  394 => 100,  392 => 99,  386 => 98,  381 => 95,  372 => 93,  363 => 92,  360 => 91,  349 => 89,  345 => 88,  332 => 86,  328 => 85,  321 => 83,  312 => 82,  309 => 81,  291 => 80,  285 => 78,  283 => 77,  280 => 76,  271 => 74,  267 => 73,  260 => 72,  258 => 71,  255 => 70,  251 => 68,  242 => 66,  238 => 65,  235 => 64,  226 => 60,  222 => 58,  208 => 57,  200 => 55,  198 => 54,  189 => 53,  186 => 52,  169 => 51,  166 => 50,  163 => 49,  145 => 47,  143 => 46,  137 => 45,  129 => 43,  125 => 42,  122 => 41,  113 => 39,  109 => 38,  102 => 37,  99 => 36,  95 => 35,  89 => 33,  87 => 32,  84 => 31,  69 => 29,  50 => 27,  48 => 26,  44 => 25,  41 => 24,  38 => 23,  34 => 22,  28 => 19,  22 => 18,  19 => 17,);
    }
}
/* {% macro renderMultiLevelMenu(menu) %}*/
/*   {% for item in menu.subcategories %}*/
/*     {% set submenu = _self.renderMultiLevelMenu(item) %}*/
/*     <li>*/
/*       <a {% if item.href %} href="{{ item.href }}" {% if item.target %} {{ item.target }} {% endif %} {% endif %} class="{{ item.class }}">*/
/*         {{ item.name }}*/
/*         {% if item.subcategories | length > 0 %}*/
/*           <i class="menu-plus"></i>*/
/*         {% endif %}*/
/*       </a>*/
/*       {% if item.subcategories | length > 0 %}*/
/*         <ul> {{ submenu }}</ul>*/
/*       {% endif %}*/
/*     </li>*/
/*   {% endfor %}*/
/* {% endmacro %}*/
/* */
/* <div id="flyout-{{ module }}" class="flyout-{{ module_id }} box flyout-menu">*/
/*   <div class="box-heading">{{ heading_title }}</div>*/
/*   <div class="flyout">*/
/*     <ul>*/
/*       {% for menu_item in menu_items %}*/
/*         {% set menu = menu_item.type == 'drop-down' ? (menu_item.subcategories | length > 0) : true %}*/
/* */
/*         <li class="fly-{{ menu_item.type }}">*/
/*           {% if menu_item.href %}*/
/*             <a href="{{ menu_item.href }}" {{ menu_item.target }}>{{ menu_item.icon }} {% if not menu_item.hide_text %}<span class="main-menu-text">{{ menu_item.name }}</span>{% endif %}{% if menu %}<i class="menu-plus"></i>{% endif %}</a><span class="clear"></span>*/
/*           {% else %}*/
/*             <a>{{ menu_item.icon }}{% if not menu_item.hide_text %}<span class="main-menu-text">{{ menu_item.name }}</span>{% endif %} {% if menu %}<i class="menu-plus"></i>{% endif %}</a><span class="clear"></span>*/
/*           {% endif %}*/
/* */
/*           {% if menu_item.type == 'mega-menu-mixed' and menu_item.mixed_columns | length > 0 %}*/
/*             <div class="fly-mega-menu mega-menu" style="{{ menu_item.css }}">*/
/*               <div>*/
/*                 {% for menu_column in menu_item.mixed_columns %}*/
/*                   {% if menu_column.type == 'mega-menu-categories' and menu_column.items | length > 0 %}*/
/*                     <div class="fly-column mega-menu-column mega-menu-categories {{ menu_column.class }}" style="width: {{ menu_column.width }}">*/
/*                       {% for cms_block in menu_column.top_cms_blocks %}*/
/*                         <div class="menu-cms-block">{{ cms_block.content }}</div>*/
/*                       {% endfor %}*/
/*                       <div>*/
/*                         {% for submenu_item in menu_column.items %}*/
/*                           <div class="mega-menu-item {{ menu_column.classes }} {{ menu_column.show_class }}">*/
/*                             <div>*/
/*                               <h3><a href="{{ submenu_item.href }}">{{ submenu_item.name }}</a></h3>*/
/*                               {% if menu_column.show in ['image', 'both'] %}*/
/*                                 <a href="{{ submenu_item.href }}"><img width="{{ submenu_item.image_width }}" height="{{ submenu_item.image_height }}" class="lazy" src="{{ submenu_item.dummy }}" data-src="{{ submenu_item.image }}" data-default-src="{{ submenu_item.image }}" alt="{{ submenu_item.name }}"/></a>*/
/*                               {% endif %}*/
/*                               {% if menu_column.show in ['links', 'both'] %}*/
/*                                 <ul>*/
/*                                   {% for sub2menu_item in submenu_item.items %}*/
/*                                     {% if menu_column.limit == 0 or (menu_column.limit > 0 and menu_column.limit >= loop.index) %}*/
/*                                       <li data-image="{{ sub2menu_item.image }}"><a href="{{ sub2menu_item.href }}">{{ sub2menu_item.name }}</a></li>*/
/*                                     {% elseif menu_column.limit > 0 and menu_column.limit == loop.index %}*/
/*                                       <li class="view-more"><a href="{{ submenu_item.href }}">{{ journal2.settings.get('view_more_text') }}</a></li>*/
/*                                     {% endif %}*/
/*                                   {% endfor %}*/
/*                                 </ul>*/
/*                               {% endif %}*/
/*                               <span class="clearfix"> </span>*/
/*                             </div>*/
/*                           </div>*/
/*                         {% endfor %}*/
/*                       </div>*/
/*                       {% for cms_block in menu_column.bottom_cms_blocks %}*/
/*                         <div class="menu-cms-block">{{ cms_block.content }}</div>*/
/*                       {% endfor %}*/
/*                     </div>*/
/*                   {% endif %}*/
/* */
/*                   {% if menu_column.type == 'mega-menu-products' and menu_column.items | length > 0 %}*/
/*                     <div class="fly-column mega-menu-column mega-menu-products {{ menu_column.class }}" style="width: {{ menu_column.width }}">*/
/*                       {% for cms_block in menu_column.top_cms_blocks %}*/
/*                         <div class="menu-cms-block">{{ cms_block.content }}</div>*/
/*                       {% endfor %}*/
/*                       <div>*/
/*                         {% if menu_column.name %}*/
/*                           <h3>{{ menu_column.name }}</h3>*/
/*                         {% endif %}*/
/*                         {% for product in menu_column.items %}*/
/*                           {% if menu_column.limit == 0 or (menu_column.limit > 0 and menu_column.limit >= loop.index) %}*/
/*                             <div class="mega-menu-item product-grid-item {{ menu_column.classes }} display-{{ journal2.settings.get('product_grid_wishlist_icon_display') }} {{ journal2.settings.get('product_grid_button_block_button') }}">*/
/*                               <div class="product-wrapper {% if product.labels and product.labels.outofstock %} outofstock {% endif %}">*/
/*                                 <div class="image">*/
/*                                   <a href="{{ product.href }}">*/
/*                                     <img class="lazy" src="{{ product.dummy }}" data-src="{{ product.image }}" width="{{ product.image_width }}" height="{{ product.image_height }}" alt="{{ product.name }}"/>*/
/*                                   </a>*/
/*                                   {% for label, name in product.labels %}*/
/*                                     <span class="label-{{ label }}"><b>{{ name }}</b></span>*/
/*                                   {% endfor %}*/
/*                                   {% if journal2.settings.get('product_grid_wishlist_icon_position') == 'image' and journal2.settings.get('product_grid_wishlist_icon_display') == 'icon' %}*/
/*                                     <div class="wishlist"><a onclick="addToWishList('{{ product.product_id }}');" class="hint--top" data-hint="{{ button_wishlist }}"><i class="wishlist-icon"></i><span class="button-wishlist-text">{{ button_wishlist }}</span></a></div>*/
/*                                     <div class="compare"><a onclick="addToCompare('{{ product.product_id }}');" class="hint--top" data-hint="{{ button_compare }}"><i class="compare-icon"></i><span class="button-compare-text">{{ button_compare }}</span></a></div>*/
/*                                   {% endif %}*/
/*                                 </div>*/
/*                                 <div class="product-details">*/
/*                                   <div class="caption">*/
/*                                     <div class="name"><a href="{{ product.href }}">{{ product.name }}</a></div>*/
/*                                     {% if product.price %}*/
/*                                       <div class="price">*/
/*                                         {% if not product.special %}*/
/*                                           {{ product.price }}*/
/*                                         {% else %}*/
/*                                           <span class="price-old">{{ product.price }}</span> <span class="price-new">{{ product.special }}</span>*/
/*                                         {% endif %}*/
/*                                       </div>*/
/*                                     {% endif %}*/
/*                                     {% if product.rating %}*/
/*                                       <div class="rating">*/
/*                                         {% for i in 1..5 %}*/
/*                                           {% if product.rating < i %}*/
/*                                             <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>*/
/*                                           {% else %}*/
/*                                             <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>*/
/*                                           {% endif %}*/
/*                                         {% endfor %}*/
/*                                       </div>*/
/*                                     {% endif %}*/
/*                                   </div>*/
/*                                   <div class="button-group">*/
/*                                     {% if staticCall('Journal2Utils', 'isEnquiryProduct', [null, product]) %}*/
/*                                       <div class="cart enquiry-button">*/
/*                                         <a href="javascript:Journal.openPopup('{{ journal2.settings.get('enquiry_popup_code') }}', '{{ product.product_id }}');" data-clk="addToCart('{{ product.product_id }}');" class="button hint--top" data-hint="{{ journal2.settings.get('enquiry_button_text') }}">{{ journal2.settings.get('enquiry_button_icon') }}<span class="button-cart-text">{{ journal2.settings.get('enquiry_button_text') }}</span></a>*/
/*                                       </div>*/
/*                                     {% else %}*/
/*                                       <div class="cart {% if product.labels and product.labels.outofstock %} outofstock {% endif %}">*/
/*                                         <a onclick="addToCart('{{ product.product_id }}', '{{ product.minimum }}');" class="button hint--top" data-hint="{{ button_cart }}"><i class="button-left-icon"></i><span class="button-cart-text">{{ button_cart }}</span><i class="button-right-icon"></i></a>*/
/*                                       </div>*/
/*                                     {% endif %}*/
/*                                     <div class="wishlist"><a onclick="addToWishList('{{ product.product_id }}');" class="hint--top" data-hint="{{ button_wishlist }}"><i class="wishlist-icon"></i><span class="button-wishlist-text">{{ button_wishlist }}</span></a></div>*/
/*                                     <div class="compare"><a onclick="addToCompare('{{ product.product_id }}');" class="hint--top" data-hint="{{ button_compare }}"><i class="compare-icon"></i><span class="button-compare-text">{{ button_compare }}</span></a></div>*/
/*                                   </div>*/
/*                                 </div>*/
/*                               </div>*/
/*                             </div>*/
/*                           {% endif %}*/
/*                         {% endfor %}*/
/*                       </div>*/
/*                       {% for cms_block in menu_column.bottom_cms_blocks %}*/
/*                         <div class="menu-cms-block">{{ cms_block.content }}</div>*/
/*                       {% endfor %}*/
/*                     </div>*/
/*                   {% endif %}*/
/* */
/*                   {% if menu_column.type == 'mega-menu-brands' and menu_column.items | length > 0 %}*/
/*                     <div class="fly-column mega-menu-column mega-menu-brands {{ menu_column.class }}" style="width: {{ menu_column.width }}">*/
/*                       {% for cms_block in menu_column.top_cms_blocks %}*/
/*                         <div class="menu-cms-block">{{ cms_block.content }}</div>*/
/*                       {% endfor %}*/
/*                       <div>*/
/*                         {% if menu_column.name %}*/
/*                           <h3>{{ menu_column.name }}</h3>*/
/*                         {% endif %}*/
/*                         {% for submenu_item in menu_column.items %}*/
/*                           <div class="mega-menu-item {{ menu_column.classes }}">*/
/*                             <div>*/
/*                               {% if submenu_item.show != 'image' and submenu_item.name %}*/
/*                                 <h3><a href="{{ submenu_item.href }}">{{ submenu_item.name }}</a></h3>*/
/*                               {% endif %}*/
/*                               <div>*/
/*                                 {% if submenu_item.show != 'text' %}*/
/*                                   <a href="{{ submenu_item.href }}"> <img width="{{ submenu_item.image_width }}" height="{{ submenu_item.image_height }}" class="lazy" src="{{ submenu_item.dummy }}" data-src="{{ submenu_item.image }}" alt="{{ submenu_item.name }}"/></a>*/
/*                                 {% endif %}*/
/*                                 <ul>*/
/*                                   {% for sub2menu_item in submenu_item.items %}*/
/*                                     <li data-image="{{ sub2menu_item.image }}"><a href="{{ sub2menu_item.href }}">{{ sub2menu_item.name }}</a></li>*/
/*                                   {% endfor %}*/
/*                                 </ul>*/
/*                               </div>*/
/*                               <span class="clearfix"></span>*/
/*                             </div>*/
/*                           </div>*/
/*                         {% endfor %}*/
/*                       </div>*/
/*                       {% for cms_block in menu_column.bottom_cms_blocks %}*/
/*                         <div class="menu-cms-block">{{ cms_block.content }}</div>*/
/*                       {% endfor %}*/
/*                     </div>*/
/*                   {% endif %}*/
/* */
/*                   {% if menu_column.type == 'mega-menu-html-block' %}*/
/*                     <div class="fly-column mega-menu-column mega-menu-html mega-menu-html-block {{ menu_column.class }}" style="width: {{ menu_column.width }}">*/
/*                       {% for cms_block in menu_column.top_cms_blocks %}*/
/*                         <div class="menu-cms-block">{{ cms_block.content }}</div>*/
/*                       {% endfor %}*/
/*                       <div>*/
/*                         {% if menu_column.name %}*/
/*                           <h3>{{ menu_column.name }}</h3>*/
/*                         {% endif %}*/
/*                         <div class="wrapper">*/
/*                           {{ menu_column.html_text }}*/
/*                         </div>*/
/*                       </div>*/
/*                       {% for cms_block in menu_column.bottom_cms_blocks %}*/
/*                         <div class="menu-cms-block">{{ cms_block.content }}</div>*/
/*                       {% endfor %}*/
/*                     </div>*/
/*                   {% endif %}*/
/*                 {% endfor %}*/
/*               </div>*/
/*             </div>*/
/*           {% endif %}*/
/* */
/*           {% if menu_item.type == 'drop-down' and menu_item.subcategories | length > 0 %}*/
/*             <ul>*/
/*               {{ _self.renderMultiLevelMenu(menu_item) }}*/
/*             </ul>*/
/*           {% endif %}*/
/*         </li>*/
/*       {% endfor %}*/
/*     </ul>*/
/*   </div>*/
/*   <script>*/
/*     $(window).resize(function () {*/
/*       $('#flyout-{{ module }} .fly-mega-menu').css('max-width', $(window).width() - 260);*/
/*     });*/
/*   </script>*/
/* </div>*/
/* */
